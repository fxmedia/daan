import path from 'path';

/*
| Optional folder location
|
*/

/*
 |--------------------------------------------------------------------------
 | Application routes
 |--------------------------------------------------------------------------
 |
 | Setup application routes here
 |
 */
export default {

    /*
    | SASS Files to compile
    */
    SASS_FILES: 'resources/sass/**/*.scss',

    /*
    | Location of the compiled sass files
    */
    STYLESHEET_DEST: 'dist/stylesheets/',

    /*
    | Path to images
    */
    IMAGES: 'resources/images/**/*.*',

    /*
    | Location of optimized/moved images
    */
    IMAGE_DEST: 'dist/images/',

    /*
    | All javascript files for watching to compile the bundles
    */
    JAVASCRIPT_FILES: 'resources/javascript/**/*.js',

    /*
    | Webpack routes
    */
    webpack_entry:  {
        /*
        | Client (mobile) Bundle
        */
        client:  './resources/javascript/client/main.js',
        /*
        | Display bundle
        */
        display: './resources/javascript/display/main.js'
    },

    /*
    | Webpack output location & filenames
    */
    webpack_output: {
        path:     path.join(__dirname, "dist/javascript/" ),
        filename: "build.[name].js"
    }


};