### Install
```
npm install -g && npm install -g gulp-cli 
npm install
npm link
```

### Other tasks
```
generate:project
```

### Build Tasks
```
// Main tasks:
gulp dev // Default task
gulp dist // Adds distribution settings like Uglify

// Subtasks:
gulp clean // Removes all built files
gulp watch // Watches resources & builds on change
gulp build // Build all resources
gulp images // Move & optinally optimize images
gulp styling // Sass compile & autoprefixer
gulp bundles // Compile javascript bundles with webpack
```

