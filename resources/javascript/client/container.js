import tEn from './translations/en';
import tNl from './translations/nl';
import modules from './require';

let _translations = {
    en: tEn,
    nl: tNl
};

let tWarnings = [],
    componentCache = {},
    pageCache = {};

function getTranslations () {
    if(_translations.language) {
        return _translations[language];
    }

    console.warn('Translations for language [%s] not found', language);
    return {};
}

var translations = getTranslations();

module.exports = React.createClass({
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getComponent: function (component, state) {
        state = state || {};

        if(componentCache[component]) {
            var CachedElement = componentCache[component];
            return <CachedElement
                t={this.translate}
                getComponent={this.getComponent}
                getPage={this.getPage}
                checkStage={this.checkStage}
                isUserAllowed={this.isUserAllowed}
                state={state}
            />
        }

        let _component;
        if ( modules.hasOwnProperty('components/' + component + '.js') ) {
            _component = modules[ 'components/' + component + '.js' ];
        }


        if ( _component.hasOwnProperty('render') ) {
            var Element = React.createClass(_component);
            componentCache[component] = Element;
            return <Element
                t={this.translate}
                getComponent={this.getComponent}
                getPage={this.getPage}
                checkStage={this.checkStage}
                isUserAllowed={this.isUserAllowed}
                state={state}
            />
        }

        return _component;
    },

    getPage: function (page, state) {
        state = state || {};

        if(pageCache[page]) {
            var CachedElement = pageCache[page];
            return <CachedElement
                t={this.translate}
                getComponent={this.getComponent}
                getPage={this.getPage}
                checkStage={this.checkStage}
                isUserAllowed={this.isUserAllowed}
                state={state}
            />
        }

        let _page;
        if ( modules.hasOwnProperty(page + '.js') ) {
            _page = modules[ page + '.js' ];
        }

        if(!_page) throw new Error('Page ['+page+'] not found in modules', page);

        var Element = React.createClass(_page);
        pageCache[page] = Element;
        return <Element
            t={this.translate}
            getComponent={this.getComponent}
            getPage={this.getPage}
            checkStage={this.checkStage}
            isUserAllowed={this.isUserAllowed}
            state={state}
        />
    },

    isUserAllowed: function (module) {
        switch ( module.visible_for ) {
            case 'anonymous':
                return user === '';
            case 'registered':
                return token !== '0' && user !== '';
            case 'none':
                return false;
            default:
                return true;
        }
    },

    checkStage: function (module) {
        switch ( this.state.client.stage ) {
            case 0:
                return module.before === 1;
            case 1:
                return module.during === 1;
            case 2:
                return module.after === 1;
            default:
                console.error('Stage not found [%s]', this.state.client.stage);
                break;
        }
    },

    translate: function (s) {
        if ( translations.hasOwnProperty(s) ) {
            return translations[ s ];
        }

        if(tWarnings.indexOf(s) === -1) {
            tWarnings.push(s);
            console.warn('Undefined translation [%s]', s);
        }

        return s;
    },

    render: function () {
        return this.getPage(this.state.client.active_page);
    }
});