if(!console){console = {};}
if(!console.log){
    console.log = function () {};
    console.clear = function() {};
    console.error = function() {};
}
console.clear();
// camelCase strings
String.prototype.toCamelCase = function() {
    return this.trim().toLowerCase().replace(/[^a-zA-Z0-9\s]/g,' ').split(/[\s]{1,}/).map((a, i) => i > 0 ? a.charAt(0).toUpperCase() + a.substr(1) : a ).join('');
};

import Connection from './ClientConnection';

// Require actions
import actions from './actions/actions';

// Require stores
import Quiz from './stores/quiz';
import Client from './stores/client';
import Questionnaire from './stores/questionnaire';
import OpenQuestion from './stores/open-question';
import Program from './stores/program';

// Initialize stores
var
    stores = {
        Quiz: new Quiz(),
        Client: new Client(),
        Questionnaire: new Questionnaire(),
        OpenQuestion: new OpenQuestion(),
        Program: new Program()
    };

// Start flux
var flux = new Fluxxor.Flux(stores, actions);

// Log all actions
flux.on("dispatch", function (type, payload) {
    console.log('dispatch [%s]: ', type, payload);
});

// Render the application
var container = require('./container');

var el = document.body;
React.render(React.createElement(container, {flux: flux}), el);

Connection
    .init()
    .start();

// debug
window.flux = flux;
window.c = Connection;

