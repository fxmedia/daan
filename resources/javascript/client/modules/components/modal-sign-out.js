var Invite = require('./../../functions/invite');

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    signOutHandler: function () {
        $('#modal--sign-out__sign-out-btn').addClass('hide');
        $('#modal--sign-out .loader').removeClass('hide');

        Invite.signOut(() => {
            $('#modal--sign-out__content-default').addClass('hide');
            $('#modal--sign-out__content-success').removeClass('hide');

            this.props.state.setUserIsAttending(false);
            this.props.state.setUserIsWaiting(false);
            grecaptcha.reset();
        }, () => {
            $('#modal--sign-out__content-default').addClass('hide');
            $('#modal--sign-out__content-fail').removeClass('hide');
        }, grecaptcha.getResponse());

    },

    backToSignout() {
        $('#modal--sign-out__sign-out-btn').removeClass('hide');
        $('#modal--sign-out .loader').addClass('hide');
        $('#modal--sign-out__content-fail').addClass('hide');
        $('#modal--sign-out__content-default').removeClass('hide');
    },

    componentDidMount() {
        $('#modal--sign-out').on('hidden.bs.modal', function () {
            this.resetModal();
        }.bind(this));

    },

    resetModal:function () {
        $('#modal--sign-out__sign-out-btn').removeClass('hide');
        $('#modal--sign-out .loader').addClass('hide');
        $('#modal--sign-out__content-default').removeClass('hide');
        $('.modal--sign-out--step2').addClass('hide');
    },

    render() {
        return <div className="modal fade modal--sign-out" id="modal--sign-out" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="close close--gray" data-dismiss="modal" aria-label="Close"></div>

                    <div id="modal--sign-out__content-default">
                        <h2>Hi {this.state.client.user.firstname} { this.state.client.user.lastname },<br/>{this.props.t('sign_out_body')}</h2>
                        <div className="g-recaptcha" data-sitekey="6Ld0SwwUAAAAAAn-IpK7ph4g9xdOTwmqwvP7tSEc"></div>
                        <div id="modal--sign-out__sign-out-btn" onClick={this.signOutHandler} className="button button--primary">{this.props.t('sign_out_btn_label')}</div>
                        <div style={{position: 'relative', textAlign: 'center', marginTop: '20px'}} className="loader hide">
                            <img src="/images/_default_/spin.svg" /> 
                        </div>
                    </div>

                    <div id="modal--sign-out__content-success" className="modal--sign-out--step2 hide">
                        <h2>{this.props.t('sign_out_successful_header')}</h2>
                        <div type="button" className="button button--primary" data-dismiss="modal">{this.props.t('close_btn_label')}</div>
                    </div>

                    <div id="modal--sign-out__content-fail" className="modal--sign-out--step2 hide">
                        <h2>Oh, no…</h2>
                        <p>Please complete the captcha.</p>
                        <div type="button" className="button button--primary" onClick={this.backToSignout}>{this.props.t('try_again_btn_label')}</div>
                    </div>
                </div>
            </div>
        </div>
    }

};