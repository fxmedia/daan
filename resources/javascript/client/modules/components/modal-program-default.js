module.exports = {

    render() {
        return <div className="modal fade modal--program-default modal--side" id="modal--program-default" tabindex="-1"
                    role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="close" data-dismiss="modal" aria-label="Close"></div>

                    <div className="modal__banner">
                        <p>About this speaker</p>
                    </div>

                    <div className="modal--program__img">
                        <img src="images/fx/dummy-image.png" alt=""/>
                    </div>

                    <div className="modal--program__content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium enim esse excepturi,
                            facilis fugiat illo iure maiores minima minus nam nobis numquam, odio officia officiis
                            perferendis possimus praesentium quaerat quasi ratione recusandae repellendus rerum sed
                            tempore, tenetur vel voluptatem voluptatum?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti est fugiat harum laborum laudantium nostrum quia quibusdam quos ratione vero.</p>
                    </div>
                </div>
            </div>
        </div>
    }

};