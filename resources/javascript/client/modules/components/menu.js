var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {
            menuActive: false
        }
    },

    componentDidMount() {

    },

    buildMenu: function () {
        var menu = [];
        modules = _.sortByOrder(modules, ['order']);
        _.each(modules, function (module) {
            if (!module.is_standalone && module.name !== '' && this.props.checkStage(module) && this.props.isUserAllowed(module)) {
                menu.push({name: module.name, path: module.path})
            }
        }.bind(this));

        // Only when more then 1 menu item
        return menu.length > 1
            ? menu
            : [];
    },


    animateToAnchor: function (event) {
        $('html,body').stop().animate({
            scrollTop: $( $.attr(event.currentTarget, 'href') ).offset().top - 60
        }, 800);
    },

    logoClickHandler() {
        if (this.props.state.page === 'home') {
            $('html, body').stop().animate({ scrollTop: 0 }, 800);
        } else {
            this.props.state.switchPage('home');
        }
    },

    renderMenuContainer() {
        var menuActiveClass = this.state.menuActive ? 'active main-menu' : 'main-menu';
        var menu = this.buildMenu();

        if (this.props.state.page === 'home') {
            if (menu.length == 0) {
                return null;
            }

            return <div id="main-menu" className={menuActiveClass} onClick={this.toggleMenu}>
                <ul className="main-menu__list">
                    {_.map(menu, function (module, key) {
                        return <a href={'#' + module['name'].toLowerCase()} onClick={this.animateToAnchor}>
                            <li key={key} >{module['name']}</li>
                        </a>
                    }.bind(this))}
                </ul>
            </div>
        }

        return false;
    },

    renderNavBarBtn() {
        if (this.props.state.page === 'home') {
            return <div id="navbar__btn" className={"navbar__btn " + (this.state.menuActive ? 'active' : '')} onClick={this.toggleMenu}></div>
        }

        return <div className="close close--gray" onClick={this.props.state.switchPage.bind(null, 'home')}></div>;
    },


    renderMenu: function () {
        return <div>
            <nav className="navbar navbar-fixed-top">
                <div className="navbar__container">
                    <div className="navbar__logo__wrapper">
                            <img src={"images/" + folder + "/logo.png"} alt="" onClick={this.logoClickHandler}/>
                    </div>
                </div>

                { this.renderNavBarBtn() }
            </nav>

            { this.renderMenuContainer() }

        </div>
    },

    toggleMenu: function () {
        this.setState({
            menuActive: !this.state.menuActive
        });
    },

    render() {
        return this.renderMenu();
    }

};