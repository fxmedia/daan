var _ = require('lodash');
var Invite = require('./../../functions/invite');
var fields = [
    {
        id: 'firstname', label: 'First name', placeholder: 'Your first name'
    },
    {
        id: 'lastname',
        label: 'Last Name',
        placeholder: 'Your last name'
    },
    {
        id: 'email', label: 'E-mail address', placeholder: 'Your email address'
    },
    {
        id: 'phonenumber',
        label: 'Mobile number',
        placeholder: 'Your mobile number'
    },
    {
        id: 'company',
        label: 'Company name',
        placeholder: 'Your company name'
    }
];

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    componentDidMount() {
        this.initializeSignUpHandling();

        $('input[type=text]').keydown(function () {
            $(this).removeClass('form-control--alert');
            $(this).parent().find('.empty-form-alert').addClass('hide');
            $(this).parent().find('.validation-alert').addClass('hide');
        });

        $('input[type=checkbox]').click(function () {
            $(this).parent().find('.empty-form-alert').addClass('hide');
        });

        $('#modal--sign-up').on('hidden.bs.modal', function () {
            this.resetModal();
        }.bind(this));
    },

    validateEmail: function (email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    },

    initializeSignUpHandling: function () {
        var self = this;

        $('#sign-up-form').on("submit", function (e) {
            e.preventDefault();

            self.clearErrors();

            $('#modal--sign-up__sign-up-btn').addClass('hide');
            $('#modal--sign-up .loader').removeClass('hide');

            var data = {};
            $(this).serializeArray().map(function (x) {
                data[x.name] = x.value;
            });

            var validationFailed = false;

            if (!$('#termsconditions1').is(':checked')) {
                $('#unchecked-termsconditions1').removeClass('hide');
                validationFailed = true;
            }

            if (validationFailed) {
                $('#modal--sign-up__sign-up-btn').removeClass('hide');
                $('#modal--sign-up .loader').addClass('hide');
                return false;
            }

            if (userIsAnonymous) {
                Invite.signUpAndRegister(data, (data) => {

                    setTimeout(function () {
                        window.location.href = data.token;
                    }, 1000);

                }, () => {

                    setTimeout(function (data) {
                        window.location.href = data.token;
                    }, 1000);

                }, (errors) => {

                    self.displayErrors(errors);

                })
            } else {
                Invite.signUp(data, () => {
                    $('#modal--sign-up__content-success').removeClass('hide');
                    $('#modal--sign-up__content-default').addClass('hide');
                    self.props.state.setUserIsAttending(true);

                }, () => {
                    $('#modal--sign-up__content-waiting').removeClass('hide');
                    $('#modal--sign-up__content-default').addClass('hide');
                    self.props.state.setUserIsWaiting(true);

                }, (errors) => {

                    self.displayErrors(errors);

                });
            }
        });
    },

    clearErrors: function () {
        let $validationAlerts = $('.validation-alert');

        $validationAlerts.each((index, element) => {
            element.parentNode.removeChild(element);
        });


    },

    displayErrors: function (errors) {
        this.resetModal();

        $.each(errors.responseJSON.errors, function (key, value) {
            let validationAlert = '<div class="validation-alert">' + value + '</div>',
                element = $('#sign-up-form').find('[name=' + key + ']');

            element.parent().append(validationAlert);
            element.addClass('form-control--alert');
        });
    },

    resetModal: function () {
        $('#modal--sign-up__sign-up-btn').removeClass('hide');
        $('#modal--sign-up .loader').addClass('hide');
        $('#modal--sign-up__content-default').removeClass('hide');
        $('.modal--sign-up--step2').addClass('hide');
    },

    renderField: function (field, index) {
        var completed = false;
        var value = this.state.client.user[field.id];

        if (this.state.client.user.hasOwnProperty(field.id) && value != '') {
            completed = true;
        }

        if (userIsAnonymous || this.state.client.user.hasOwnProperty(field.id)) {
            return <div className="form-group">
                <label for="firstname">{ field.label }</label>
                <div className="form-input-container">
                    <input disabled={completed ? "disabled" : false} type="text" className="form-control" id={"field-" + field.id} name={field.id}
                           placeholder={field.placeholder} defaultValue={value} onBlur={this.onFieldBlur}/>
                    { this.props.getComponent('tick-animation', {active: completed})}
                </div>
            </div>
        }

        return false;
    },

    onFieldBlur: function () {
        //TODO: vaidation check


    },

    renderFormIntro() {
        if (userIsAnonymous) {
            return <div className="modal--sign-up__intro"><h2>Hi</h2><p>Please fill in everything</p></div>
        }

        return <div className="modal--sign-up__intro"><h2>Hi {this.state.client.user.firstname} { this.state.client.user.lastname }</h2>
            <p>{this.props.t('sign_up_body')} If your current details are incorrect, please email us at <a
            href="mailto:info@fxmedia.nl">info@fxmedia.nl</a></p></div>
    },

    render() {
        return <div className="modal fade modal--sign-up" id="modal--sign-up" tabindex="-1" role="dialog"
                    aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="close close--gray" data-dismiss="modal" aria-label="Close"></div>

                    <div id="modal--sign-up__content-default">

                        { this.renderFormIntro() }

                        <form id="sign-up-form" method="POST">

                            { _.map(fields, this.renderField) }

                            <div className="form-group form-group-with-checkbox">
                                <input type="checkbox" className="form-control--checkbox" id="termsconditions1"
                                       name="termsconditions1"/>
                                <label for="termsconditions1">I agree that the information as filled in above, including
                                    my mobile number, will be used before, during and after the event strictly for
                                    communication purposes about this event.</label>
                                <div id="unchecked-termsconditions1" className="hide empty-form-alert">You must agree to
                                    be able to attend this event.
                                </div>
                            </div>

                            <input id="modal--sign-up__sign-up-btn" className="button button--primary" type="submit"
                                   value={this.props.t('sign_up_btn_label')}/>
                            <div style={{position: 'relative', textAlign: 'center', marginTop: '20px'}}
                                 className="loader hide">
                                <img src="/images/_default_/loader.gif"/>
                            </div>
                        </form>
                    </div>

                    <div id="modal--sign-up__content-success" className="modal--sign-up--step2 hide">
                        <h2>{this.props.t('sign_up_successful_header')}</h2>
                        <p>{this.props.t('sign_up_successful_body')}</p>
                        <div type="button" className="button button--primary"
                             data-dismiss="modal">{this.props.t('close_btn_label')}</div>
                    </div>

                    <div id="modal--sign-up__content-waiting" className="modal--sign-up--step2 hide">
                        <h2>{this.props.t('sign_up_waiting_header')}</h2>
                        <p>{this.props.t('sign_up_waiting_body')}</p>
                        <div type="button" className="button button--primary"
                             data-dismiss="modal">{this.props.t('close_btn_label')}</div>
                    </div>

                    <div id="modal--sign-up__content-fail" className="modal--sign-up--step2 hide">
                        <h2>Oh, no…</h2>
                        <p>Something went wrong, please try again.</p>
                        <div type="button" className="button button--primary"
                             data-dismiss="modal">{this.props.t('close_btn_label')}</div>
                    </div>
                </div>
            </div>
        </div>
    }

};

