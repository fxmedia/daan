var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    getScoreboardComponent() {
        if (this.props.state.type !== 'quiz') {
            return false;
        }

        return this.props.getComponent('scoreboard');
    },

    render() {
        return <header className="header__wrap">
            <div className="l-half logo__wrap">
                <img src={"images/"+folder+"/logo.png"} alt="Logo" />
            </div>
            { this.getScoreboardComponent() }
            <div className="l-clear"></div>
        </header>
    }

};