var Connection = require('./../../ClientConnection'),
    _ = require('lodash');

module.exports = {

    getInitialState() {
        var focus = this.props.state.data.hasOwnProperty('id') ? this.props.state.data : false;
        return {
            phonebook: {},
            search: '',
            focus: focus
        };
    },

    componentWillMount() {
        Connection.getPhonebookData(data => {
            var phonebook = {};
            var sortedData = data;

            sortedData.sort(function (a, b) {

                if (!a.hasOwnProperty('lastname')) {
                    a.lastname = '';
                }

                if (!b.hasOwnProperty('lastname')) {
                    b.lastname = '';
                }

                var chunksA = a.lastname.split(" "),
                    chunksB = b.lastname.split(" "),
                    partA = chunksA[chunksA.length - 1],
                    partB = chunksB[chunksB.length - 1];

                partA = partA.toUpperCase();
                partB = partB.toUpperCase();

                if (partA < partB) {
                    return -1;
                }
                if (partA > partB) {
                    return 1;
                }

                return 0;
            });

            _.each(sortedData, function (row) {
                var chunks = row.lastname.split(" "),
                    part = chunks[chunks.length - 1],
                    letter = part.charAt(0).toUpperCase().charCodeAt(0);

                if (!phonebook.hasOwnProperty(letter)) {
                    phonebook[letter] = [];
                }

                phonebook[letter].push(row);
            });

            this.setState({phonebook: phonebook})
        });
    },

    search(event) {
        var el = event.currentTarget;

        // ESC
        if (event.keyCode && event.keyCode === 27) {
            this.refs.searchInput.getDOMNode().value = '';
            return this.setState({search: ''});
        }

        this.setState({search: el.value});
    },

    renderPhonebook(people, char) {
        var rows = [];

        _.each(people, row => {
            var name = row.firstname + ' ' + row.lastname;
            var reg = new RegExp(this.state.search, 'gi');

            if (this.state.search.length > 0 && name.search(reg) === -1) {

            } else {
                rows.push(row);
            }
        });

        if (rows.length > 0) {
            return <div>
                <div className="separator">{String.fromCharCode(char)}</div>
                {_.map(rows, this.renderRow)}
            </div>
        }

        return <div></div>
    },

    focusPerson(person) {
        this.setState({focus: person});
    },

    closeFocus() {
        this.setState({focus: false});
    },

    renderFocus() {
        if (!this.state.focus) {
            return null;
        }

        return this.props.getComponent('phonebook-focus', {
            person: this.state.focus,
            closeFocus: this.closeFocus,
            close: this.props.state.setPhonebookModalOpen.bind(null, false),
            switchPage: this.props.state.switchPage
        });
    },

    renderPicture(image) {
        var avatarType = "image image__default";
        var style = {};

        if (image !== '') {
            avatarType = "image";

            style = {
                backgroundImage: 'url(' + image + ')'
            };
        }

        return <div className="image-wrapper">
            <div className={ avatarType } style={ style }></div>
        </div>
    },

    renderRow(row) {
        return <div onClick={this.focusPerson.bind(this, row)} className="phonebook-row">
            {this.renderPicture(row.image)}
            <div className="content">
                <div className="name">{row.firstname} {row.lastname}</div>
            </div>
        </div>
    },

    focusSearch() {
        this.refs.searchInput.getDOMNode().focus();
    },

    render() {
        return <div>
            <div className="popup-overlay" onClick={this.props.state.setPhonebookModalOpen.bind(null, false)}></div>

            <div className="popup phonebook-popup phonebook-wrapper">

                <div className="close" onClick={this.props.state.setPhonebookModalOpen.bind(null, false)}></div>

                <div className="modal__banner">
                    <p>{this.props.t('pages_header_phonebook')}</p>
                </div>

                <div className="modal__content">

                    <div onClick={this.focusSearch} className="search-wrapper">
                        <div className="search-field">
                            <div className="search-icon"></div>
                            <input onKeyUp={this.search} type="text" ref="searchInput"
                                   placeholder={this.props.t('phonebook_search_placeholder')}/>
                        </div>
                    </div>

                    <div className="phonebook-popup__people">

                    {function () {
                        if (Object.keys(this.state.phonebook).length === 0) {
                            return <div
                                style={{position: 'absolute', left: '50%', transform: 'translateX(-50%)', marginTop: '20px'}}>
                                <img src="/images/_default_/loader.gif"/>
                            </div>
                        }

                        return _.map(this.state.phonebook, this.renderPhonebook)
                    }.bind(this)() }
                    </div>

                </div>

                {this.renderFocus()}
            </div>
        </div>
    }

};