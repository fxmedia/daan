var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    renderDescription() {
        // var descWithNewLine = this.props.state.description.split("\n").join("<br />");
        // return <div dangerouslySetInnerHTML={this.unsafe(descWithNewLine)}></div>;
    },

    // unsafe(html){
    //     return {__html: html}
    // },

    renderOpenModalButton() {
        if (this.props.state.hasOwnProperty('modal')) {
            return <div className="more-info-btn" type="button" data-toggle="modal"
                        data-target={"#modal--" + this.props.state.modal}>More info <span
                className="arrow"></span></div>
        }

        return false;
    },

    render() {
        return <div className={"swiper-slide swiper-slide--day" + this.props.state.day}>
            <div className="program">
                <div className="program__header">
                    <div className="program__img-container with-icon"
                         style={{backgroundImage: 'url(' + this.props.state.image +')' }}>
                    </div>
                    <img className="white-gradient-overlay"
                         src="/images/_default_/gradient-square.png"/>
                </div>
                <div className="program__info">
                    <div className="program__time"><p>{ this.props.state.time }</p></div>
                    <h3>{ this.props.state.title }</h3>
                    <h4>{ this.props.state.subtitle }</h4>
                    { this.renderOpenModalButton() }
                </div>

                <div className="clearfix"></div>
            </div>
        </div>
    }

};