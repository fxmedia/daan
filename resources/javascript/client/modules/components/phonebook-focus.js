var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {

        }
    },

    componentWillMount() {

    },

    renderPicture() {
        var avatarType = "image image__default";
        var style = {};

        if ( this.props.state.person.image !== '' ) {
            avatarType = "image";

            style = {
                backgroundImage: 'url(' + this.props.state.person.image + ')'
            };
        }

        return <div className="person__image">
            <div className="image-wrapper with-border-and-shadow">
                <div className={ avatarType } style={ style }></div>
            </div>
        </div>
    },

    render() {
        var person = this.props.state.person;

        return <div>
            <div className="phonebook-popup-focused popup">
                <div className="person__header">
                    <img className="white-gradient-overlay" src="/images/_default_/gradient-square.png" />

                    { this.renderPicture() }
                    <h3>{person.firstname} {person.lastname} {function () {
                        return person.token === token ? '(You)' : ''
                    }()}</h3>

                </div>
                <div className="back" onClick={this.props.state.closeFocus}></div>
                <div className="close" onClick={this.props.state.close}></div>
                <div className="person__body">
                    <div className="person__body__section">
                        <h3>{this.props.t('person_phonenumber_label')}</h3>
                        <p><a href={'tel:' + person.phonenumber}>{person.phonenumber}</a></p>
                    </div>
                    <div className="person__body__section">
                        <h3>{this.props.t('person_email_label')}</h3>
                        <p><a href={'mailto:' + person.email}>{person.email}</a></p>
                    </div>
                </div>
            </div>
        </div>
    }

};