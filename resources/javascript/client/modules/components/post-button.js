class PostButton extends React.Component {

    constructor(props) {
        super(props);
    }

    setPostsModalOpen() {
        this.props.modalHandler(this.props.buttonType);
    }

    render() {
      return (
          <div buttonType={this.props.buttonType} className={this.props.className} onClick={this.setPostsModalOpen.bind(this)}>
              <div className="button--post__label">
                  <p>{this.props.message}</p>
                  <p>{this.props.bodyText}</p>
              </div>
          </div>
      )
    }
}

module.exports = PostButton;