module.exports = {

    handleInfoButtonClick: function() {
        

        $('.ad__popup').toggleClass('active')
    },

    handleCloseButtonClick: function() {
        

        $('.ad__popup').toggleClass('active');
    },

    render: function() {
        
        
        return <div className="ad__wrap">
            <div className="ad__popup">
                <div onClick={this.handleCloseButtonClick} className="button remove-btn"></div>
                <div className="ad__popup__logo"><img src={"images/_default_/replylive-logo.png"} alt="A quiz by reply.live"/></div>
                <h2>{ this.props.t('ad_banner_header') }</h2>
                <p>{ this.props.t('ad_banner_body') }</p>
                <p><a target="_blank" href="https://reply.live">{ this.props.t('ad_banner_link') }</a></p>
            </div>
            <div onClick={this.handleInfoButtonClick} className="ad__banner">
                <div className="ad__banner__logo">
                    <span className="ad__banner__label">{ this.props.t('ad_banner_label')}</span>
                    <img src={"images/_default_/replylive-logo-blue.png"} alt="reply.live logo"/>
                </div>
                <div className="ad__button"><img src={"images/_default_/info-icon.png"} alt="info"/></div>
            </div>
        </div>
    }
};
