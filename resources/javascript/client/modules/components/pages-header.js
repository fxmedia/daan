var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    render() {
        return <div className="page__header">

            <div className="page__header__container">

                <h1>{this.props.state.title}</h1>
                <p>{this.props.state.paragraph}</p>
                
            </div>

        </div>
    }

};