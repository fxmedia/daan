var _ = require('lodash');
var Connection = require('./../../ClientConnection');
var ProgressBar = require('progressbar.js');
var wallFileupload = require('./../../functions/fileupload-wall.function.js');
var userFileupload = require('./../../functions/fileupload-user.function.js');

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState() {
        return {
            viewing: (this.props.state.data.viewing) ? this.props.state.data.viewing : 'comments',
            error: '',
            info: '',
            input_message: '',
            max_char: 1000,
            char_left: 1000,
            page_state: this.getSingularType(this.props.state.postType),
            file_name: '',
            file_path: '',
            preview: '',
            file_select_open: false,
            user_file_name: '',
            uploading: false
        }
    },

    getSingularType(postType) {
        return postType.substr(0, postType.length-1);
    },

    componentDidMount: function () {
        // Create a progressbar for the upload of a user image & file image
        this.progressbar = new ProgressBar.Circle('#posts-progressbar-container',
            this.getProgressbarOptions()
        );

        setTimeout(function () {
            this.attachUserimageUpload();
        }.bind(this), 500);

        wallFileupload(this);
    },


    /**
     *
     *  Handling user message editing and submitting
     *
     *  */

    renderEditMessageQuestion: function () {
        var questionColumn = (this.state.client.settings.question_column == "1");
        var pd = questionColumn ? {} : {paddingTop: '10px'};

        return <div className="posts__message-editor" style={pd}>
            <div onClick={this.clickInputImage} className="add-photo__wrap">
                {this.renderPhoto()}

                <div className="progressbar-container" id="posts-progressbar-container"></div>
            </div>
            {this.renderMessage()}
            {this.renderError()}
            <div className="posts__textarea-wrap">
            <textarea onKeyUp={this.charInput} name="" id="text-input" cols="20" rows="3"
                      placeholder={this.props.t('placeholder_input_text')}/>
            </div>
            <div onClick={this.submitMessage}
                 className="button button--primary">{this.props.t('send_button_text')}</div>
            <span id="some-element">
                            <input type="file" name="file" id="input_image" className="something"
                                   style={{ opacity: 0, width: 0, height: 0, position:'absolute', left: 0, bottom: 0 }}/>
                        </span>
        </div>
    },

    /* Submitting photo and files handling*/
    renderPhoto: function () {
        if (this.state.preview.length > 10 && this.state.error == '') {
            return <div className="add-photo--edit">
                <div onClick={this.removePhoto} className="remove-btn"></div>
                <div className="button__image" style={{ backgroundImage: 'url(' + this.state.preview + ')' }} onClick={this.changePhoto}></div>
            </div>
        }

        return <div className="add-photo__placeholder"><p>{this.props.t('upload_media_placeholder_text')}</p></div>
    },

    changePhoto: function () {
        $('#input_image').click();
    },

    removePhoto: function () {
        this.setState({
            file_name: '',
            file_path: '',
            preview: '',
            info: '',
            error: ''
        });
    },

    attachUserimageUpload: function () {
        userFileupload(this);
    },

    clickInputImage: function () {
        if (this.state.file_path.length > 10) {
            return;
        }

        this.setState({
            info: '',
            error: ''
        });

        $('#input_image').click();
    },

    getProgressbarOptions: function () {
        return {
            color: '#EEE',
            strokeWidth: 4,
            trailWidth: 1,
            duration: 1000,
            trailColor: 'rgba(0,0,0,0)',
            text: {
                value: '0',
                style: {
                    fontSize: '30px'
                }
            },
            fill: 'rgba(0, 0, 0, 0.4)',
            step: function (state, bar) {
                bar.setText((bar.value() * 100).toFixed(0));
            }
        };
    },

    isVideo: function (file) {
        return !!file.match(/\.(mp4|mkv|avi|mov|webm|wmv|m4v)$/);
    },

    /* Error handling */
    renderError: function () {
        if (this.state.error.length == 0) {
            return false;
        }

        return <div onClick={this.dismissError} className="info__wrap info__wrap--error">
            <div className="remove-btn"></div>
            <p>{this.state.error}</p>
        </div>
    },

    showError: function (message) {
        console.error(message);

        this.setState({
            error: message
        });
    },

    dismissError: function () {
        this.setState({
            error: ''
        });
    },

    /* Message handling */
    renderMessage: function () {
        if (this.state.info.length == 0) {
            return false;
        }

        return <div onClick={this.dismissMessage} className="info__wrap info__wrap--message">
            <div className="remove-btn"></div>
            <p>{this.state.info}</p>
        </div>
    },

    renderStaticQuestionHeader() {
        let message = "You can only post anonymous questions here. Whenever you post a question, this will only be visible to you, not others.";
        return (
            <div data-post-id="999999" className={'message__list__item'}>
                <div className="message__body">
                    <div className="name">Asics</div>
                    <div className="message__body__text"
                         dangerouslySetInnerHTML={{__html: this.replaceURLWithHTMLLinks(message)}}/>
                </div>
                <div className="l-clear"></div>
            </div>
        )
    },

    charInput: function (e) {
        var el = e.currentTarget,
            left = (this.state.max_char - el.value.length);

        if (left < 0) {
            document.getElementById('text-input').placeholder = this.props.t('input_to_many_characters');
        } else {
            document.getElementById('text-input').placeholder = '';
        }

        this.setState({
            char_left: left,
            input_message: el.value,
            info: '',
            error: ''
        });
    },

    submitMessage: function (e) {
        var input = document.getElementById('text-input');
        var message = input.value;

        var data = {
            message: message,
            state: this.state.page_state,
            file: this.state.file_name,
            file_path: this.state.file_path,
            post_on_twitter: $('#post_on_twitter').prop('checked')
        };

        if (!data.message && !data.file_path) {
            this.showError(this.props.t('empty_text_and_media'));
            return;
        }

        if (message.length > this.state.max_char) {
            this.showError(this.props.t('input_to_many_characters'));
            return;
        }

        this.showMessage(this.props.t('message_sending'));


        Connection.wallPost(data, function (result) {

            if (result.success) {

                this.removePhoto();
                this.showMessage(this.props.t('post_submitted'));
                

            } else {
                this.showError(result);
            }

            input.value = '';

        }.bind(this));
    },

    showMessage: function (message) {
        this.setState({
            info: message
        });
    },

    dismissMessage: function () {
        this.setState({
            info: ''
        });
    },


    /**
     *  Handling the rendering of the message board
     *
     *  */

    renderUserPicture: function (user) {
        var avatarType = "image image__default";
        var style = {};

        if (user.image !== '') {
            avatarType = "image";

            style = {
                backgroundImage: 'url(' + user.image + ')'
            };
        }

        return <div className="image-wrapper">
            <div className={avatarType} style={style}></div>
        </div>
    },

    renderUserInfo: function (user) {
        return <div className="message__thumb">
            { this.renderUserPicture(user) }
            <div className="name">{user.firstname} {user.lastname}</div>
        </div>
    },

    replaceURLWithHTMLLinks(text) {
        var exp = /(\b(https?|www|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
        return text.replace(exp, "<a href='$1' target='_blank'>$1</a>");
    },

    renderMessageRow: function (post) {
        let image = this.getImage(post),
            message = post.message ? <p>{this.replaceURLWithHTMLLinks(post.message)}</p> : false,
            me = (this.state.client.user.id === post.user.id) ? ' me' : '';

        // timestamp is used in isotope for sorting - post-id un-used
        return <div data-post-id={post.id} data-timestamp={post.timestamp} className={'message__list__item' + me}>
            {this.renderUserInfo(post.user)}
            <div className="message__body">
                <div className="message__body__text"
                     dangerouslySetInnerHTML={{__html: this.replaceURLWithHTMLLinks(post.message)}}/>
                {image}
            </div>
            <div className="l-clear"></div>
        </div>
    },

    getImage: function (post) {
        // objects contains movie, thumb and ani-gif
        if (post) {
            if (typeof(post.file) == 'object') {
                if (post.file) {
                    return <img src={post.file.gif} alt="Movie"/>
                }
            }
        }

        if (post.file_path) {
            return <img src={post.file_path} alt="Picture"/>
        }

        if (post.file) {
            return <img src={post.file} alt="Animated GIF"/>
        }

        return false;
    },

    showMessages() {
        this.setState({viewing: 'comments'});
    },

    showQuestions() {
        this.setState({viewing: 'questions'});
    },

    renderMessages(questions, comments) {
        if (this.state.page_state === 'question') {
            return _.map(questions, this.renderMessageRow);
        } else {
            return _.map(comments, this.renderMessageRow);
        }
    },

    renderMessageChoice(questions, comments) {
        return false; //TODO: remove this later?


        if (!questions.length || !comments.length) {
            return null;
        }

        var activeMessage = this.state.viewing === 'comments' ? ' active' : '';
        var activeQuestions = this.state.viewing === 'questions' ? ' active' : '';

        return <div className="message__choice">
            <div onClick={this.showMessages} className={"button block" + activeMessage}>Messages</div>
            <div onClick={this.showQuestions} className={"button block" + activeQuestions}>Questions</div>
            <div className="clearfix"></div>
        </div>
    },

    render() {
        var questions = _.filter(this.state.client.posts, {type: 'question', user_id: this.state.client.user.id});
        var comments = _.filter(this.state.client.posts, function (post) {
            return (!post.type || post.type == 'comment' || post.type == 'photo')
        });


        return <div>
            <div className="popup-overlay" onClick={this.props.state.setPostsModalOpen.bind(null, false)}></div>

            <div className="popup posts-popup posts">

                <div className="close" onClick={this.props.state.setPostsModalOpen.bind(null, false)}></div>

                <div className="modal__banner">
                    <p>{this.props.t('pages_header_posts')}</p>
                </div>


                <div className="modal__content">
                    {this.renderEditMessageQuestion()}

                    {this.renderMessageChoice(questions, comments)}

                    <div className="posts__container">
                        { this.state.page_state === 'question' ? this.renderStaticQuestionHeader() : ""}
                        { this.renderMessages(questions, comments)}
                    </div>
                </div>


            </div>
        </div>

    }

};