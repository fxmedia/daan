var _ = require('lodash');

module.exports = {

    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState() {
        return {}
    },

    componentWillMount() {



    },

    render() {
        if (userIsAnonymous) { return false }


        return <div className="modal fade modal--ticket" id="modal--ticket" tabindex="-1" role="dialog" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content text-center">
                    <div className="close" data-dismiss="modal" aria-label="Close"></div>

                    <div className="ticket-icon">
                    </div>

                    <h2>Reply.live</h2>
                    <p>00 Month & 00 Month 2017, Location</p>

                    <div className="animated-circAndTick hide">
                        <div className="trigger"></div>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlnsXlink="http://www.w3.org/1999/xlink"
                             x="0px" y="0px"
                             viewBox="0 0 37 37" style={{enableBackground: 'new 0 0 37 37'}} xmlSpace="preserve">
                            <path className="animated-circ path"
                                  style={{fill:'none',stroke:'#fff',strokeWidth:'3',strokeLinejoin:'round',strokeMiterlimit:'10'}} d="
	M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z"
                            />
                            <polyline className="animated-tick path"
                                      style={{fill:'none', stroke:'#fff', strokeWidth:'3', strokeLinejoin:'round', strokeMiterlimit:'10'}}
                                      points="
	11.6,20 15.9,24.2 26.4,13.8 "/>
                        </svg>
                    </div>

                    <div className="user-barcode"><img
                        src={ base_url + "barcode?proxy=http://www.barcodes4.me/barcode/c128b/" + token + ".png?width=360" }/>
                    </div>

                    <h1>{this.state.client.user.firstname} { this.state.client.user.lastname }</h1>
                </div>

            </div>
        </div>
    }

};