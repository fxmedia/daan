var _           = require('lodash');

var questionnaire = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin('Client', 'Questionnaire')
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            client: Flux.store('Client').getState(),
            questionnaire: Flux.store('Questionnaire').getState()
        };
    },

    getInitialState: function () {
        return {
            questionnairesCompleted: false
        };
    },

    setActive: function(questionnaire) {
        if (this.answered(questionnaire) == questionnaire.questions.length) {
            return false;
        }

        this.getFlux().store('Questionnaire').setActive(questionnaire.id);
        this.getFlux().store('Client').setPage('questionnaire');
    },

    answered: function(questionnaire) {
        return _.intersection(this.state.questionnaire.answers, _.map(questionnaire.questions, 'id' )).length;
    },

    getTotalQuestions: function() {
        return _.sum(_.map(this.state.questionnaire.questionnaires, function (questionnaire) {
            return questionnaire.questions.length;
        }));
    },
    
    renderConfirmationText: function() {
        if (this.state.questionnaire.answers.length < this.getTotalQuestions() || this.state.questionnaire.answers.length == 0) {
            return <section></section>
        }

        return <section className="content__wrap content__wrap--client">
            <div className="intro__wrap__body questionnaire__confirmation">
                <p>The winner will be announced on Wednesday June 29th at 17:30.</p>
            </div>
        </section>
    },

    renderQuestionnaires: function() {
        return this.state.questionnaire.questionnaires.map(function(q){
            var votedClass = '';

            if (this.answered(q) == q.questions.length) {
                votedClass = 'voted';
            }

            return <div className={"questionnaire__start-btn " + votedClass} onClick={this.setActive.bind(this, q)}>
                    <div className="questionnaire__start-btn__overlay">

                    </div>
                    <div className="questionnaire__start-btn__inner">
                        <h1>{q.name}</h1>
                        <p>Please vote on your favourite finalist.</p>
                    </div>
                </div>
        }.bind(this));
    },

    render: function() {
        return <div className="wrapper">
            <div className="l-container--mobile">
                <header className="header__wrap">
                    <div className="logo__wrap">
                        <img src={"images/"+folder+"/logo.png"} alt="Logo"/>
                    </div>
                    <div className="l-clear"></div>
                </header>
                <section className="content__wrap content__wrap--client">
                    <div className="intro__wrap__body">
                        <h1>{'WELCOME '}<br/>{this.state.client.user.firstname}</h1>
                    </div>
                </section>
                <section className="content__wrap content__wrap--questionnaires">
                    {this.renderQuestionnaires()}
                </section>
                {this.renderConfirmationText()}
            </div>
        </div>
    }
};

module.exports = questionnaire;