var _ = require('lodash');
var Connection = require('./../../ClientConnection');

var questionnaire = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin('Client', 'Questionnaire')
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            client: Flux.store('Client').getState(),
            questionnaire: Flux.store('Questionnaire').getState()
        };
    },

    getInitialState: function () {
        return {
            questionnaireStarted: false,
            selectedAnswer: false,
            selectedAnswers: []
        };
    },

    getQuestionnaire: function () {
        this.state.questionnaire.questionnaire.only_questions = this.state.questionnaire.questionnaire.outro == "0";

        return this.state.questionnaire.questionnaire;
    },

    getQuestion: function () {
        return this.state.questionnaire.questionnaire.questions[this.getUserAnswers().length] || {answers: {}};
    },

    getUserAnswers: function () {
        //return this.state.questionnaire.answers;
        return _.intersection(this.state.questionnaire.answers, _.map(this.getQuestionnaire().questions, 'id'));
    },

    lastQuestion: function () {
        return this.getUserAnswers().length == this.getQuestionnaire().questions.length;
    },

    renderMultipleType: function () {
        var answers = $.extend({}, this.getQuestion().answers);
        delete answers.answer_type;

        return <div className="questionnaire__answer--abcd questionnaire__answer--multiple">
            <ol className="list__answer list__answer--abcd list__answer list__answer--multiple">
                {_.map(answers, this.renderMultipleAnswer)}
            </ol>
            <div onClick={this.confirmMultipleAnswer}
                 className="button button--primary">{this.props.t('label_next_question')}</div>
        </div>
    },

    confirmMultipleAnswer() {
        Connection.questionnaireAnswer(this.getQuestion().id, this.state.selectedAnswers, this.getQuestionnaire().id);
        this.getFlux().store('Questionnaire').setAnswer(this.getQuestion().id);

        this.setState({selectedAnswers: null});

        if (this.getQuestionnaire().only_questions == 1 && this.lastQuestion()) {
            this.gotoWall();
        }
    },

    renderMultipleAnswer: function (answer, key) {
        if (answer.length < 1) return null;

        var active = '';

        if (this.state.selectedAnswers.indexOf(key) > -1) {
            active = 'active';
        }

        return <li onClick={this.toggleMultipleAnswer.bind(this, key)} id={'answer_' + key} key={key}>
            <a className={active}>
                {answer}
            </a>
        </li>
    },

    renderAbcdType: function () {
        var answers = $.extend({}, this.getQuestion().answers);
        delete answers.answer_type;

        return <div className="questionnaire__answer--abcd">
            <ol className="list__answer list__answer--abcd ">
                {_.map(answers, this.renderAbcdAnswer)}
            </ol>
        </div>
    },

    renderAbcdAnswer: function (answer, key) {
        if (answer.length < 1) return null;

        var active = '';

        if (key == this.state.selectedAnswer) {
            active = 'active';
        }

        return <li onClick={this.setAnswer.bind(this, key)} id={'answer_' + key} key={key}>
            <a className={active}>
                {answer}
                {this.renderConfirmBox()}
            </a>
        </li>
    },

    renderConfirmBox: function () {
        return <div className="confirmBox">
            <div className="decline" onClick={this.declineAbcdAnswer}>
                <span className="icon icon__decline">{this.props.t('confirm_decline')}</span>
            </div>
            <div className="confirm" onClick={this.confirmAbcdTypeAnswer}>
                <span className="icon icon__confirm">{this.props.t('confirm_confirm')}</span>
            </div>
        </div>
    },

    declineAbcdAnswer: function (event) {
        event.preventDefault();
        event.stopPropagation();

        this.setState({selectedAnswer: null});
    },

    confirmAbcdTypeAnswer: function (event) {
        event.preventDefault();
        event.stopPropagation();

        Connection.questionnaireAnswer(this.getQuestion().id, this.state.selectedAnswer);
        this.getFlux().store('Questionnaire').setAnswer(this.getQuestion().id);

        this.setState({selectedAnswer: null});

        if (this.getQuestionnaire().only_questions == 1 && this.lastQuestion()) {
            this.gotoWall();
        }
    },

    renderOpenType: function () {
        return <div className="questionnaire__answer--open">
            <textarea onKeyUp={this.charInput} name="" id="questionnaire-text-input" cols="27" rows="5"
                      placeholder={this.props.t('questionnaire_placeholder_input_text')}/>
            <div onClick={this.confirmOpenTypeAnswer} className="button button--primary">{this.props.t('label_next_question')}</div>
        </div>
    },

    confirmOpenTypeAnswer: function () {
        if ($('#questionnaire-text-input').val()) {
            Connection.questionnaireAnswer(this.getQuestion().id, $('#questionnaire-text-input').val());
            this.getFlux().store('Questionnaire').setAnswer(this.getQuestion().id);

            if (this.getQuestionnaire().only_questions == 1 && this.lastQuestion()) {
                this.gotoWall();
            }

            $('html,body').scrollTop(0);
        }

        $('#questionnaire-text-input').val("");
    },

    renderRangeType: function () {
        return <div className="questionnaire__answer--range">
            <div className="slider"></div>
            <div onClick={this.confirmRangeTypeAnswer} className="button button--primary">{this.props.t('label_next_question')}</div>
        </div>
    },

    confirmRangeTypeAnswer: function () {
        Connection.questionnaireAnswer(this.getQuestion().id, $('.slider').slider("option", "value"));
        this.getFlux().store('Questionnaire').setAnswer(this.getQuestion().id);

        if (this.getQuestionnaire().only_questions == 1 && this.lastQuestion()) {
            this.gotoWall();
        }
    },

    startQuestionnaire: function () {
        this.setState({questionnaireStarted: true});
    },

    setAnswer: function (answer) {
        this.setState({selectedAnswer: answer});
    },

    toggleMultipleAnswer: function (answer) {
        var answers = this.state.selectedAnswers,
            match = answers.indexOf(answer);

        if (match !== -1) {
            answers.splice(match, 1);
        } else {
            answers.push(answer);
        }

        this.setState({selectedAnswers: answers});
    },

    renderProgressBar: function () {
        if (this.getQuestionnaire().questions.length <= 1) {
            return null;
        }

        var progress = (( this.getUserAnswers().length + 1 ) / this.getQuestionnaire().questions.length ) * 100;

        return <div>
            <div className="questionnaire-progress-bar">
                <span className="progress-bar__inner" style={{width: (progress + '%')}}></span>
            </div>
            <h3>Question {this.getUserAnswers().length + 1} of {this.getQuestionnaire().questions.length}</h3>
        </div>
    },

    renderQuestionsAnswers: function () {
        if (!this.getQuestion() || !this.getQuestion().hasOwnProperty('answers')) {
            return <section className="content__wrap text-center">
            </section>
        }

        var renderAnswerType;

        switch (this.getQuestion().answers.answer_type) {
            case 'multiple':
                renderAnswerType = this.renderMultipleType();
                break;
            case "abcd":
                renderAnswerType = this.renderAbcdType();
                break;
            case "open":
                renderAnswerType = this.renderOpenType();
                break;
            case "range":
                renderAnswerType = this.renderRangeType();
                break;
        }


        return <section className="content__wrap text-center content__wrap--questionnaire">
            <div className="question__wrap question__wrap--questionnaire">
                {this.renderProgressBar()}
                <p>{this.getQuestion().question}</p>
            </div>
            <div className="questionnaire__answer">
                {renderAnswerType}
            </div>
        </section>
    },

    renderIntro: function () {
        return <section className="content__wrap text-center questionnaire__intro">
            <p>{this.props.t('questionnaire_intro_body')}</p>
            <div onClick={this.startQuestionnaire} className="button button--primary">{this.props.t('label_start_survey')}</div>
        </section>
    },

    renderOutro: function () {
        return <section className="content__wrap text-center questionnaire__outro">
            <div className="ribon__wrap ribon__wrap--questionnaire">
                <h1>{this.props.t('label_thanks') + ' ' + this.state.client.user.firstname}</h1>
            </div>
            <p>{this.props.t('questionnaire_outro_body')}</p>
            <div onClick={this.gotoWall} className="button button--primary">{this.props.t('label_back')}</div>
        </section>
    },

    gotoWall: function () {
        this.getFlux().actions.setSubPage('home');
        this.getFlux().actions.setPage('general/home');
    },

    componentDidUpdate: function () {
        this.startSlider();
    },

    componentDidMount: function () {
        this.startSlider();
    },

    startSlider: function () {
        if ($('.slider').length > 0) {
            $('.slider__label').remove();

            $('.slider').slider({
                value: 3,
                min: parseInt(this.getQuestion().answers['min_range']),
                max: parseInt(this.getQuestion().answers['max_range']),
                step: 1
            }).each(function () {

                // Add labels to slider whose values
                // are specified by min, max

                // Get the options for this slider (specified above)
                var opt = $(this).data().uiSlider.options;

                // Get the number of possible values
                var vals = opt.max - opt.min;

                // Position the labels
                for (var i = 0; i <= vals; i++) {
                    // Create a new element and position it with percentages
                    var el = $('<label class="slider__label">' + (i + opt.min) + '</label>').css('left', (i / vals * 100) + '%');

                    // Add the element inside #slider
                    $('.slider').append(el);
                }
            });
        }
    },


    render: function () {
        var renderMain = this.renderQuestionsAnswers();

        if (this.getQuestionnaire().intro == 1 && this.getUserAnswers().length < 1 && !this.state.questionnaireStarted) {
            renderMain = this.renderIntro();
        }

        if (this.getQuestionnaire().outro == 1 && this.getUserAnswers().length == this.getQuestionnaire().questions.length) {
            renderMain = this.renderOutro();
        }

        if (this.lastQuestion() && this.getQuestionnaire().outro == 0 ) {
            setTimeout(() => this.gotoWall(), 0);
        }

        return <div className="wrapper wrapper--questionnaire">

            <div className="l-container l-container--mobile">

                { this.props.getComponent('modules-header') }
                
                {renderMain}

            </div>
        </div>
    }
};

module.exports = questionnaire;
