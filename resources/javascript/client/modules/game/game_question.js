var _           = require('lodash');
var Connection = require('./../../ClientConnection');

var game_question = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Quiz", "Client")
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();

        Flux.store('Quiz').on('change_question', this.goBack);

        return {
            quiz: Flux.store('Quiz').getState(),
            client: Flux.store('Client').getState()
        };
    },

    setAnswer: function (answer, e) {

        e.preventDefault();

        var $wrapper = $('.wrapper');
        $wrapper.css('transition', 'background .5s ease-in');

        if(answer === 'a') {
            $wrapper.css('background', 'url(../../images/'+folder+'/pick-1.jpg) no-repeat top center');
            $wrapper.css('background-size', 'cover');
        } else {
            $wrapper.css('background', 'url(../../images/'+folder+'/pick-2.jpg) no-repeat top center');
            $wrapper.css('background-size', 'cover');
        }

        //$('.logo__wrap').animate({opacity: 0}, 500);
        //$('#choice-label').html(translation.label_wait_question);

        this.getFlux().store('Quiz').setMyAnswer(answer);
    },

    renderAnswer: function (answer, key) {
        if(answer.length < 1) return null;

        var active = (this.state.quiz.my_answer == key) ? 'active ' : '';
        if(this.isValidated && key == this.state.quiz.answer) {
            active += 'correct';
        }

        return <li key={key}>
            <a onClick={this.setAnswer.bind(this, key)} href="#" id={"btnAnswer_" + key}><span className="sr-only">{answer}</span></a>
        </li>
    },

    isCorrect: function () {
        return (this.state.quiz.my_answer && (this.state.quiz.my_answer == this.state.quiz.answer))
    },

    isValidated: function () {
        return this.state.quiz.answer ? true : false;
    },

    layOver: function() {
        var status, label, render = false;

        if(this.isValidated() && this.isCorrect()) {
            status = 'right';
            label = this.props.t('label_correct_answer');
            render = true;
        } else if (this.isValidated() && this.state.quiz.my_answer !== null ) {
            status = 'wrong';
            label = this.props.t('label_wrong_answer');
            render = true;
        }

        if( render ) {
            return <section className="content__wrap text-center status">
                <div className={'answer__wrap answer__wrap--' + status}>
                    <div className="l-container">
                        <div className="answer__wrap__icon">
                            {this.renderIcon(status)}
                        </div>
                        <div className="ribon__wrap ribon__wrap--20">
                            <h1>{label}</h1>
                        </div>
                    </div>
                </div>
            </section>
        }
    },

    renderIcon: function(status) {
        if(status == 'wrong') {
            return <svg style={{fill: 'white'}} viewBox="0 0 48.1 46.5">
                <title>Wrong answer icon</title>
                <path d="M0,25.3L0,4.4C0,2.1,1.9,0.2,4.2,0l7.6,0c2.1,0,3.7,1.9,3.7,4.4v20.9c0,2.3-1.5,4.7-3.7,4.7H4.1C2,30-0.1,27.7,0,25.3z
		 M4.1,2C2.9,2.2,2,3.2,2,4.4v20.9C2,26.6,2.9,27.7,4.2,28h7.6c0.9,0,1.7-1.3,1.7-2.7V4.4c0-1.3-0.7-2.4-1.7-2.4H4.1z"/>
                <path d="M26.4,46.5c-3-0.2-5.3-2.7-5.1-5.7c0-0.1,0-0.3,0-0.4c0-0.2,0-0.5,0-0.7c0.8-3.6-0.8-7.7-1.9-9.8h-0.9
		c-2.3-0.2-4.1-2.2-4.2-4.5V4.6c0.2-2.5,2.3-4.5,4.8-4.5h24c0.9,0.1,1.7,0.4,2.4,0.9c2.5,1.7,3.1,5,1.4,7.5c0.7,0.9,1.1,2,1.1,3.2
		c0,1.1-0.3,2.3-1,3.2c1.4,1.9,1.4,4.4,0,6.3c0.7,0.9,1,2.1,1,3.2c0,1.5-0.6,2.9-1.6,3.9c-1,1.1-2.4,1.7-3.8,1.7h-8.7
		c1.3,5,0.2,10.4-3,14.5L30.5,45l0,0c-1,1-2.4,1.5-3.8,1.5C26.6,46.5,26.5,46.5,26.4,46.5z M31.6,29.3L31.2,28h11.4
		c0.9,0,1.8-0.4,2.4-1.1c0.6-0.7,1-1.6,1-2.5c0-0.9-0.4-1.8-1.1-2.4l-0.7-0.8l0.8-0.7c0,0,0.1-0.1,0.1-0.1c1.3-1.4,1.3-3.5-0.1-4.8
		l-0.8-0.7l0.8-0.7c0,0,0.1-0.1,0.1-0.1c1.3-1.4,1.2-3.5-0.2-4.8l-0.8-0.7l0.7-0.9c0.5-0.5,0.8-1.2,0.9-1.9c0.3-1.9-1-3.6-2.9-3.8
		H19.1c-1.4,0.1-2.5,1.1-2.6,2.5v21c0.1,1.3,1.2,2.4,2.5,2.5h0.5c0.5,0,1,0.2,1.4,0.6v0.5H21l0.2,0.4c1.2,2.4,3,7,2.1,11.2l0,0
		c0,0.2,0,0.3,0,0.4l0,0c0,1.9,1.5,3.4,3.4,3.4c1.1,0,2-0.5,2.7-1.3l0.4-0.5C32.5,38.8,33.2,33.8,31.6,29.3z"/>
            </svg>
        } else if(status == 'right') {
            return <svg style={{fill: 'white'}} viewBox="0 0 48.1 46.5">
                <title>Right answer icon</title>
                <path d="M11.9,46.5H4.2A4.5,4.5,0,0,1,0,42.1V21.1c0-2.4,2.1-4.7,4.2-4.7h7.6c2.2,0,3.7,2.4,3.7,4.7V42.1C15.6,44.5,14,46.5,11.9,46.5Zm-7.6-28A2.9,2.9,0,0,0,2,21.1V42.1a2.5,2.5,0,0,0,2.2,2.4h7.6c1,0,1.7-1.1,1.7-2.4V21.1c0-1.4-.8-2.7-1.7-2.7H4.2Z"/>
                <path d="M42.7,46.5H19.2A4.8,4.8,0,0,1,14.4,42V21.1a4.7,4.7,0,0,1,4.2-4.5h0.9c1.1-2.1,2.7-6.2,1.9-9.8a2.2,2.2,0,0,1,0-.7c0-.1,0-0.3,0-0.4a5.4,5.4,0,0,1,9.1-3.8h0L31,2a16.5,16.5,0,0,1,3,14.5h8.8a5.4,5.4,0,0,1,3.8,1.7A5.6,5.6,0,0,1,48.1,22a5.3,5.3,0,0,1-1,3.2,5.4,5.4,0,0,1,0,6.3,5.3,5.3,0,0,1,1,3.2A5.4,5.4,0,0,1,47,38a5.4,5.4,0,0,1-3.7,8.4H42.7Zm-23.4-28H18.9a2.7,2.7,0,0,0-2.5,2.5V42a2.8,2.8,0,0,0,2.8,2.5H42.9a3.4,3.4,0,0,0,2-5.7l-0.8-.7,0.8-.7a3.4,3.4,0,0,0,.1-4.9l-0.8-.7,0.8-.7a3.4,3.4,0,0,0,0-4.9l-0.8-.7,0.8-.7A3.3,3.3,0,0,0,46.1,22a3.6,3.6,0,0,0-1-2.5,3.4,3.4,0,0,0-2.4-1.1H31.3l0.4-1.3a14.6,14.6,0,0,0-2-13.6l-0.3-.3a3.4,3.4,0,0,0-6,2.1c0,0.1,0,.2,0,0.4h0c0.9,4.2-.9,8.8-2.1,11.2l-0.2.4H21v0.2a1.8,1.8,0,0,1-1.4.6H19.3Z"/>
            </svg>
        }
        return null;
    },

    goBack: function() {
        var $wrapper = $('.wrapper');
        $wrapper.css('background', '#8E258D url(../../images/' + folder + '/bg_mobile_primary.png) no-repeat top center');
        $wrapper.css('background-size', 'cover');
        $('#choice_label').html(this.props.t('label_make_a_choice'));
        //$('.logo__wrap').animate({opacity: 1}, 500);
    },

    render: function () {
        return <div className="wrapper">
                {this.layOver()}
                <div className="l-container l-container--mobile">
                    { this.props.getComponent('modules-header') }
                    <div className="l-clear"></div>
                    <section className="content__wrap text-center">
                        <div className="ribon__wrap ribon__wrap--20 choice">
                            <h1 id="choice-label">{this.props.t('label_make_a_choice')}</h1>
                        </div>
                        <ul className="list__answer list__answer--ab">
                            {_.map(this.state.quiz.active_question.answers, this.renderAnswer)}
                        </ul>
                    </section>
                </div>

            <div className="answer__wrap game">
                <div className="l-container">
                    <a onClick={this.goBack} href="#" className="button__back">
                        <svg viewBox="0 0 5.8 7.7">
                            <title>chevron-left</title>
                            <polygon points="5.2 7.7 0 3.5 5.2 0 5.8 0.8 1.7 3.5 5.8 6.9 5.2 7.7"/>
                        </svg>
                    </a>
                    <div className="waiting__wrap">
                        <div className="ribon__wrap ribon__wrap--20 ribon__wrap--waiting">
                            <div className="icon"> </div>
                            <p>{this.props.t('label_wait_question')}</p>
                            <div className="l-clear"></div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
    }
};

module.exports = game_question;

