var ProgressBar = require('progressbar.js');
var wallFileupload = require('./../../functions/fileupload-wall.function.js');
var userFileupload = require('./../../functions/fileupload-user.function.js');
var PostButton = require('./../../modules/components/post-button.js');

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState: function () {
        return {
            file_name: '',
            file_path: '',
            file_select_open: false,
            user_file_name: '',
            uploading: false
        };
    },

    componentDidMount: function () {
        // Create a progressbar for the upload of a user image & file image
        this.progressbar = new ProgressBar.Circle('.progressbar-container',
            this.getProgressbarOptions()
        );

        setTimeout(function () {
            this.attachUserimageUpload();
        }.bind(this), 500);

        wallFileupload(this);
    },

    renderProgressbar: function () {
        var style = {
            width: '100px',
            height: '100px',
            top: '40%',
            left: '50%',
            display: 'none',
            zIndex: '9999',
            marginTop: '-150px',
            position: 'absolute',
            transform: 'translate(-50%, -50%)',
            msTransform: 'translate(-50%, -50%)',
            WebkitTransform: 'translate(-50%, -50%)'
        };

        return <div className="progressbar-container" style={style}></div>
    },

    getProgressbarOptions: function () {
        return {
            color: '#EEE',
            strokeWidth: 4,
            trailWidth: 1,
            duration: 1000,
            trailColor: 'rgba(0,0,0,0)',
            text: {
                value: '0',
                style: {
                    fontSize: '30px'
                }
            },
            fill: 'rgba(0, 0, 0, 0.4)',
            step: function (state, bar) {
                bar.setText((bar.value() * 100).toFixed(0));
            }
        };
    },

    renderPicture: function () {
        if (user == '') {
            return false;
        }

        if (this.state.client.settings.hasOwnProperty('show_photo_upload') && this.state.client.settings.show_photo_upload == "0") {
            return false;
        }

        var picture = this.state.client.user.image;
        var avatarType = "avatar avatar__default";
        var profileWrapType = "profile__wrap--default";
        var style = {};

        if (picture) {
            avatarType = "avatar";
            profileWrapType = "";

            style = {
                backgroundImage: 'url(' + picture + ')',
            };
        }

        return <div onClick={this.selectUserImage} className={"profile__wrap " + profileWrapType}>
            <div className={avatarType} alt="avatar" style={style}></div>
            <img className="picture-tag" src={"/images/_default_/camera.png"} alt="Camera"/>
            <input type="hidden" name="user_id" value={this.state.client.user.id}/>
            <input id="user_image" type="file" name="file"/>
        </div>
    },

    renderPostsButtons: function () {
        return <div className="button--post__wrap">

            <PostButton
                className={"button--post--message"}
                modalHandler={this.setPostsModalOpen}
                message={this.props.t('circle_text_message')}
                bodyText={this.props.t('circle_text_message_body')}
                buttonType={"comments"}
            />

            <PostButton
                className={"button--post--question"}
                modalHandler={this.setPostsModalOpen}
                message={this.props.t('circle_text_question')}
                bodyText={this.props.t('circle_text_question_body')}
                buttonType={"questions"}
            />
        </div>
    },


    renderInteractiveWallButtons() {
        ////////////////////// deze buttons werkene niet
        if (this.state.client.settings.question_column == "1") {
            return this.renderPostsButtons();
        }

        return this.renderDiscussButton();
    },

    selectUserImage: function () {
        $('#user_image').click();
    },

    setPostsModalOpen: function(type) {
        console.log(type);
        this.props.state.setPostsModalOpen(true, type);
    },

    attachUserimageUpload: function () {
        userFileupload(this);
    },

    renderUsername: function () {
        return <div className="profile__name--plain"><span
            className="user-name-label">{this.state.client.user.firstname} {this.state.client.user.lastname}</span>
        </div>
    },

    renderPreEventStage: function () {
        if (this.state.client.stage == 0) {
            return <div className="header--preevent-stage">

                {this.renderPreEventContent()}

            </div>
        }

        return false;
    },

    renderPreEventContent: function () {
        if (userIsAnonymous) {
            return <div className="header--preevent--anonymous">
                <div className="header__content">
                    <h1>Welcome, sign up now to join</h1>
                    <h1><b>{this.props.t('event_name')}</b></h1>
                    <h2>{this.props.t('preevent_header_h2')}</h2>

                    <p>{this.props.t('preevent_header_p')}</p>

                    <div className="button button--primary" data-toggle="modal"
                         data-target="#modal--sign-up">{this.props.t('preevent_sign_up_btn_label')}</div>
                </div>
            </div>
        }

        if (this.props.state.userIsAttending) {
            return <div className="header--preevent--attending">
                <div className="header__profile">
                    {this.renderPicture()}
                </div>

                <div className="header__content">

                    <h1>Hey {this.state.client.user.firstname} {this.state.client.user.lastname}</h1>
                    <h2>{this.props.t('preevent_attending_header_h1')}</h2>
                    <p className="small">{this.props.t('preevent_sign_out')} <span
                        className="nice-underline cursor-pointer" data-toggle="modal" data-target="#modal--sign-out">here</span>
                    </p>

                    <p>{this.props.t('preevent_header_p')}</p>
                </div>

                { this.renderDiscussButton() }

                <div>
                    { this.renderOpenTicketButton() }
                </div>

            </div>
        }

        if (this.props.state.userIsWaiting) {
            return <div className="header--preevent--waiting">
                <div className="header__content">
                    <h1>{this.props.t('preevent_waiting_header_h1')}</h1>
                    <h2>{this.props.t('preevent_header_h2')}</h2>
                    <p>{this.props.t('preevent_header_p')}</p>

                    <p className="small">{this.props.t('preevent_sign_out')} <span
                        className="nice-underline cursor-pointer"
                        data-toggle="modal"
                        data-target="#modal--sign-out">here</span>
                    </p>
                </div>
            </div>
        }

        return <div className="header--preevent--notattending">
            <div className="header__content">
                <h1>
                    Welcome {this.state.client.user.firstname} {this.state.client.user.lastname}, {this.props.t('preevent_header_h1')}</h1>
                <h1><b>{this.props.t('event_name')}</b></h1>
                <h2>{this.props.t('preevent_header_h2')}</h2>
                <p>{this.props.t('preevent_header_p')}</p>

                <div className="button button--primary" data-toggle="modal"
                     data-target="#modal--sign-up">{this.props.t('preevent_sign_up_btn_label')}</div>
            </div>
        </div>
    },

    renderDiscussButton: function () {
        console.log(this.props.state);

        if (this.props.isUserAllowed('posts')) {
            return <div onClick={this.setPostsModalOpen}
                        className="button--post--discuss">
                <div className="button--post__label">
                    <p>{this.props.t('circle_text_discuss_header')}</p>
                    <p>{this.props.t('circle_text_discuss_body')}</p>
                </div>
            </div>
        }

        return false;
    },

    renderDuringEventStage: function () {
        if (this.state.client.stage == 1) {

            return <div className="header--preevent-stage">

                <div className="header__profile">
                    {this.renderPicture()}
                </div>

                <div className="header__content">
                    <h1>{this.props.t('duringevent_header_h1')}</h1>
                    <h2>{this.props.t('duringevent_header_h2')}</h2>
                    <p>{this.props.t('duringevent_header_p')}</p>

                </div>

                { this.renderInteractiveWallButtons() }

            </div>
        }

        return false;
    },

    renderAfterEventStage: function () {
        if (this.state.client.stage == 2) {
            return <div className="header--afterevent-stage">

                <div className="header__content">
                    <h1>{this.props.t('afterevent_header_h1')}</h1>
                    <h2>{this.props.t('afterevent_header_h2')}</h2>
                </div>
            </div>
        }

        return false;
    },

    renderOpenTicketButton: function () {
        return false;

        return <div className="button button--primary" type="button" data-toggle="modal" data-target="#modal--ticket">
            Open your ticket
        </div>
    },

    render: function () {
        return <div className="container-fluid header__wrapper">
            <div id="header" className="container header">

                <section className="content__wrap content__wrap--client">
                        {this.renderPreEventStage()}
                        {this.renderDuringEventStage()}
                        {this.renderAfterEventStage()}
                </section>

                {this.renderProgressbar()}
            </div>
        </div>
    },
};