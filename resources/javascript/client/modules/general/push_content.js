var push_content = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            user: Flux.store('Client').getState().user,
            push_content: Flux.store('Client').getState().push_content
        };
    },

    renderImage() {
        if (this.state.push_content.image) {
            return <section className="content__wrap content__wrap--client">
                <img className="push__image" src={this.state.push_content.image_url} width="100%"/>
            </section>
        }
    },

    parseVariables(message) {
        message = message.replace('[firstname]', '<span class="user-name-label">' + this.state.user.firstname + '</span>');
        message = message.replace('[lastname]', '<span class="user-name-label">' + this.state.user.lastname + '</span>');

        return message;
    },

    render: function () {
        

        return <div className="wrapper wrapper--push">
            <div className="l-container l-container--mobile">
                <header className="header__wrap">
                    <div className="l-half logo__wrap">
                        <img src={"images/"+folder+"/logo.png"} alt="Logo" />
                    </div>
                </header>
                <div className="l-clear"></div>
                <div className="message__wrap">
                    <div className="message__wrap__heading">
                        <div className="profile__name ribon__wrap ribon__wrap--20"><span
                            className="welcome-label" dangerouslySetInnerHTML={{__html: this.parseVariables(this.state.push_content.name)}}></span></div>
                    </div>
                </div>

                {this.renderImage()}
            </div>
        </div>

    }
};

module.exports = push_content;