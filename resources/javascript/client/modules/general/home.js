var _ = require('lodash'),
    menuProps = {};

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState: function () {
        return {
            userIsAttending: userIsAttending,
            userIsWaiting: userIsWaiting,
            phonebookModalOpen: false,
            postsModalOpen: false
        };
    },

    switchPage: function (page, props) {
        props = props || {};

        menuProps = props;
        this.getFlux().actions.setSubPage(page.replace('.js', ''));
    },

    setUserIsAttending: function (state) {
        this.setState({userIsAttending: state});
    },

    setUserIsWaiting: function (state) {
        this.setState({userIsWaiting: state});
    },

    setPhonebookModalOpen: function (state) {
        this.setState({phonebookModalOpen: state});
    },

    renderPhonebookModal: function () {

        if (this.state.phonebookModalOpen && this.props.isUserAllowed('home-phonebook')) {

            return this.props.getComponent('phonebook', {
                switchPage: this.switchPage,
                data: menuProps,
                setPhonebookModalOpen: this.setPhonebookModalOpen
            });
        }

        return false;
    },

    setPostsModalOpen: function (state, type) {
        this.setState({postsModalOpen: state, postModalType: type});
    },

    renderPostsModal: function () {
        if (this.state.postsModalOpen && this.props.isUserAllowed('posts')) {

            return this.props.getComponent('posts', {
                switchPage: this.switchPage,
                data: menuProps,
                setPostsModalOpen: this.setPostsModalOpen,
                postType: this.state.postModalType
            });
        }

        return false;
    },

    render: function () {
        var page = this.state.client.sub_page;

        // if (user == '') {
        //     return <div>{this.props.getPage('pages/404', {})}</div>
        // }

        return <div className="main-wrapper">

            { this.props.getComponent('modal-sign-up', {
                setUserIsAttending: this.setUserIsAttending,
                setUserIsWaiting: this.setUserIsWaiting
            })}
            { this.props.getComponent('modal-sign-out', {
                setUserIsAttending: this.setUserIsAttending,
                setUserIsWaiting: this.setUserIsWaiting
            })}
            { this.props.getComponent('modal-program-default', {
                setUserIsAttending: this.setUserIsAttending,
                setUserIsWaiting: this.setUserIsWaiting
            })}
            { this.props.getComponent('modal-ticket')}
            { this.renderPhonebookModal() }
            { this.renderPostsModal() }

            { this.props.getComponent('menu', {switchPage: this.switchPage, page: page})}

            {this.props.getPage('pages/' + page, {
                switchPage: this.switchPage,
                data: menuProps,
                userIsAttending: this.state.userIsAttending,
                userIsWaiting: this.state.userIsWaiting,
                setPhonebookModalOpen: this.setPhonebookModalOpen,
                setPostsModalOpen: this.setPostsModalOpen,
            })}

        </div>

    }
};