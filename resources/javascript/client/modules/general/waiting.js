var wall = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            client: Flux.store('Client').getState()
        };
    },

    render: function() {
        return <div className="wrapper wrapper--waiting">
            <div className="l-container l-container--mobile">
                <header className="header__wrap">
                    <div className="l-half logo__wrap">
                        <img src={"images/"+folder+"/logo.png"} alt="Logo" />
                    </div>
                </header>
                <div className="l-clear"></div>
                <section className="content__wrap text-center">
                    <div className="l-container">
                        <div className="waiting__wrap__icon">
                            <svg viewBox="0 0 137.6 195.5">
                                <title>Waiting icon</title>
                                <path d="M117.9,75.2c3.6-2.5,12.1-8.5,12.2-22.9V22.7h4.1a3.4,3.4,0,0,0,3.4-3.4V3.5A3.4,3.4,0,0,0,134.2.1H3.5A3.4,3.4,0,0,0,.1,3.4V19.3a3.4,3.4,0,0,0,3.4,3.4H7.5V52.8c0,14.3,4.3,17.6,12,22.3L52.9,95.5v8.7c-8.4,5-30.4,18.3-33.2,20.3S7.5,132.9,7.5,147.3v25.5H3.4A3.4,3.4,0,0,0,0,176.1v15.9a3.4,3.4,0,0,0,3.4,3.4H134.1a3.4,3.4,0,0,0,3.4-3.4V176.2a3.4,3.4,0,0,0-3.4-3.4H130V146.8c0-14.2-4.3-17.6-12-22.3L84.7,104.2V95.5c8.4-5,30.4-18.3,33.2-20.3h0ZM75.5,112.8l36.3,22.1c2.8,1.7,4.3,2.6,5,3.7s1.2,4,1.2,8.2v26.1H19.6V147.3c0-8.1,4-10.9,7-12.9S49,120.6,62.1,112.8a6.1,6.1,0,0,0,2.9-5.2V92.1a6.1,6.1,0,0,0-2.9-5.2L26,64.8H25.8c-2.8-1.7-4.3-2.6-5-3.7s-1.2-4-1.2-8.2V22.7H118V52.4c0,8.1-4,10.9-7,12.9S88.5,79.1,75.5,86.9a6.1,6.1,0,0,0-3,5.2v15.5a6.1,6.1,0,0,0,2.9,5.2h0Zm0,0"/>
                            </svg>
                        </div>
                        <div className="ribon__wrap ribon__wrap--20 ribon__wrap--wait">
                            <h1>{ this.props.t('label_wait_question') }</h1>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    }

};

module.exports = wall;