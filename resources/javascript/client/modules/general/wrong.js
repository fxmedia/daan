var wrong = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            client: Flux.store('Client').getState()
        };
    },
    render: function() {
        return <div className="wrapper">
            <div className="l-container l-container--mobile">
                <header className="header__wrap">
                    <div className="l-half">
                        <div className="logo__wrap">
                            <img src={"images/"+folder+"/logo.png"} alt="Logo" />
                        </div>
                    </div>
                    <div className="l-clear"></div>
                </header>
                <section className="content__wrap text-center">
                    <div className="answer__wrap answer__wrap--wrong">
                        <div className="l-container">
                            <div className="answer__wrap__icon">
                                <svg style={{fill: 'white'}} viewBox="0 0 48.1 46.5">
                                    <title>Wrong answer icon</title>
                                    <path d="M0,25.3L0,4.4C0,2.1,1.9,0.2,4.2,0l7.6,0c2.1,0,3.7,1.9,3.7,4.4v20.9c0,2.3-1.5,4.7-3.7,4.7H4.1C2,30-0.1,27.7,0,25.3z
		 M4.1,2C2.9,2.2,2,3.2,2,4.4v20.9C2,26.6,2.9,27.7,4.2,28h7.6c0.9,0,1.7-1.3,1.7-2.7V4.4c0-1.3-0.7-2.4-1.7-2.4H4.1z"/>
                                    <path d="M26.4,46.5c-3-0.2-5.3-2.7-5.1-5.7c0-0.1,0-0.3,0-0.4c0-0.2,0-0.5,0-0.7c0.8-3.6-0.8-7.7-1.9-9.8h-0.9
		c-2.3-0.2-4.1-2.2-4.2-4.5V4.6c0.2-2.5,2.3-4.5,4.8-4.5h24c0.9,0.1,1.7,0.4,2.4,0.9c2.5,1.7,3.1,5,1.4,7.5c0.7,0.9,1.1,2,1.1,3.2
		c0,1.1-0.3,2.3-1,3.2c1.4,1.9,1.4,4.4,0,6.3c0.7,0.9,1,2.1,1,3.2c0,1.5-0.6,2.9-1.6,3.9c-1,1.1-2.4,1.7-3.8,1.7h-8.7
		c1.3,5,0.2,10.4-3,14.5L30.5,45l0,0c-1,1-2.4,1.5-3.8,1.5C26.6,46.5,26.5,46.5,26.4,46.5z M31.6,29.3L31.2,28h11.4
		c0.9,0,1.8-0.4,2.4-1.1c0.6-0.7,1-1.6,1-2.5c0-0.9-0.4-1.8-1.1-2.4l-0.7-0.8l0.8-0.7c0,0,0.1-0.1,0.1-0.1c1.3-1.4,1.3-3.5-0.1-4.8
		l-0.8-0.7l0.8-0.7c0,0,0.1-0.1,0.1-0.1c1.3-1.4,1.2-3.5-0.2-4.8l-0.8-0.7l0.7-0.9c0.5-0.5,0.8-1.2,0.9-1.9c0.3-1.9-1-3.6-2.9-3.8
		H19.1c-1.4,0.1-2.5,1.1-2.6,2.5v21c0.1,1.3,1.2,2.4,2.5,2.5h0.5c0.5,0,1,0.2,1.4,0.6v0.5H21l0.2,0.4c1.2,2.4,3,7,2.1,11.2l0,0
		c0,0.2,0,0.3,0,0.4l0,0c0,1.9,1.5,3.4,3.4,3.4c1.1,0,2-0.5,2.7-1.3l0.4-0.5C32.5,38.8,33.2,33.8,31.6,29.3z"/>
                                </svg>
                            </div>
                            <div className="ribon__wrap ribon__wrap--20">
                                <h1>{ this.props.t('label_wrong_answer') }</h1>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    }
};

module.exports = wrong;