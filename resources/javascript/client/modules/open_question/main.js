var _ = require('lodash');
var Connection = require('./../../ClientConnection');

var openQuestion = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin('Client', 'OpenQuestion')
    ],

    getStateFromFlux: function () {
        var Flux = this.getFlux(); 
        return {
            client: Flux.store('Client').getState(),
            open_question: Flux.store('OpenQuestion').getState()
        };
    },

    getInitialState: function () {
        return {
            question: '',
            answer_sent: false,
            maxWordsTextArea: 3,
        };
    },

    /* Textareas with wordcount */
    checkWordCount: function (event) {
        var el = $(event.currentTarget),
            maxWords = this.state.maxWordsTextArea,
            words = el.val().split(/\s+/g),
            counter = el.parent().find('.word-counter');
        if(words.length == 1 && words[0] =="") words = [];
        words.splice(maxWords);
        el.val(words.join(' '));
        // block whitespaces by too many words
        if( words.length >= maxWords){
            if( event.keyCode == 32 ||  // space
                event.keyCode == 13 ) { // enter
                event.preventDefault();
            }
            // make text red
            counter.addClass('text-error').removeClass('text-primary');
        } else {
            // keep text green
            counter.addClass('text-primary').removeClass('text-error');
        }
        // set words info box
        counter.html(Math.max(maxWords - words.length,0));
    },

    renderQuestion: function () {
        return <section className="content__wrap text-center oq-question-intro">
            <p>{this.state.open_question.oqQuestion.question}</p>
        </section>
    },

    renderOpenType: function () {
        return  <section className="content__wrap text-center oq-answer-field">
                    <textarea
                        data-max-words={this.state.maxWordsTextArea}
                        onChange={this.checkWordCount}
                        name=""
                        id="questionnaire-text-input"
                        cols="27"
                        rows="5"
                        placeholder={this.props.t('questionnaire_placeholder_input_text')}
                    />
                    <span className="word-counter">{this.state.maxWordsTextArea}</span><span> words left</span>
                    <div onClick={this.confirmOpenTypeAnswer} className="button button--primary">{"Send Answer"}</div>
                </section>
    },

    confirmOpenTypeAnswer: function () {
        console.log("Answer being sent..")
        if ($('#questionnaire-text-input').val()) {
            Connection.oqSendAnswer($('#questionnaire-text-input').val(), this.state.open_question.oqQuestion.id);
            console.log("Answer succesfully sent to server..")
            this.setState({answer_sent: true})
        } else {
            this.setState({answer_sent: false})
        }

        $('#questionnaire-text-input').val("");
    },

    renderOutro: function () {
        return <section className="content__wrap text-center questionnaire__outro">
            <div className="ribon__wrap ribon__wrap--questionnaire">
                <h1>Thanks for your answer</h1>
            </div>
        </section>
    },

    render: function () {
        var renderMain = this.renderOpenType();

        return <div className="wrapper wrapper--questionnaire">

            <div className="l-container l-container--mobile">

                { this.props.getComponent('modules-header') }
                
                {this.renderQuestion()}
                {renderMain}

                {this.state.answer_sent ? this.renderOutro() : ""}

            </div>
        </div>
    }
};

module.exports = openQuestion;
