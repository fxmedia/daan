module.exports = {

    componentDidMount() {

    },

    formSubmitHandler() {
        $('#register-form__submit-btn').addClass('hide');
        $('#register-form').find('.loader').removeClass('hide');
    },

    render: function () {
        return <div className="wrapper wrapper--register">
            <div className="l-container l-container--mobile">
                <header className="header__wrap header__wrap--register">
                    <div className="l-half">
                        <div className="logo__wrap logo--welcome">
                            <img src={"images/"+folder+"/logo.png"} alt="Logo" />
                        </div>
                    </div>
                    <div className="l-clear"></div>
                </header>

                <section className="content__wrap content__wrap--client content__wrap--register text-center">
                    <div className="l-container register__intro">
                        <div className="ribon__wrap ribon__wrap--20">
                            <h1>{this.props.t('register_welcome')}</h1>
                        </div>
                        <p className="p--register--top">{this.props.t('register_body')}</p>
                    </div>

                    <div className="form-panel">
                        <form id="register-form" method="POST" action="/register" accept-charset="UTF-8" className=""
                              enctype="multipart/form-data">

                            <div className="form-group l-full">
                                <label>{this.props.t('register_firstname')}</label>
                                <input type="text" name="firstname" className="form-control" placeholder=""
                                       maxLength="33"/>
                            </div>

                            <div className="form-group l-full">
                                <input id="register-form__submit-btn" type="submit" value={this.props.t('register_submit_btn')}
                                       className="button button--primary" onClick={this.formSubmitHandler}/>
                            </div>

                            <div style={{position: 'relative', textAlign: 'center', marginTop: '20px'}} className="loader hide">
                                <img src="/images/_default_/loader.gif" />
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    }
};