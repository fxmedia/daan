var _ = require('lodash');

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    componentDidMount() {
        this.resizeMoodImage();

        $(window).resize(function() {
            this.resizeMoodImage();
        }.bind(this));
    },

    componentDidUpdate() {
        this.resizeMoodImage();
    },

    resizeMoodImage() {
        if (window.innerWidth >= 752) {
            var aboutContentHeight = $('#about__content').parent().height();

            $('#about__mood-image').css('height', aboutContentHeight);


        } else {
            $('#about__mood-image').css('height', 200);
        }
    },

    render() {
        return <div className="home-content">
            <div id="about" className="about mood-image__wrapper">
                <div className="col-md-6">
                    <div id="about__content" className="about__content">
                        <h2>About this event</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet nam nemo quis. Beatae dolorum enim, et modi quaerat quos sint.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi error ipsa repellat?</p>
                    </div>

                    
                </div>

                <div className="col-md-6">
                    <div id="about__mood-image" className="mood-image"></div>
                </div>
            </div>
        </div>





    }

};