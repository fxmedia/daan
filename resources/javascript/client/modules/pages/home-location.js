var _ = require('lodash');
var myMap;
var myMarker;

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    componentDidMount() {
        this.initializeGoogleMaps();
        this.resizeGoogleMaps();

        $(window).resize(function() {
            this.resizeGoogleMaps();
        }.bind(this));

        this.updateGoogleMaps();
    },

    componentDidUpdate() {
        this.updateGoogleMaps();
    },

    initializeGoogleMaps() {
        var myLatLng = new google.maps.LatLng(52.093134, 5.135471);

        var mapOptions = {
            center: myLatLng,
            zoom: 15,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var markerOptions = {
            position: myLatLng,
            title: "Event"
        };

        myMap = new google.maps.Map(document.getElementById('google-maps'), mapOptions);
        myMarker = new google.maps.Marker(markerOptions).setMap(myMap);

    },

    updateGoogleMaps() {
        google.maps.event.trigger(myMap, 'resize'); //This is needed to 'refresh' google maps
    },

    resizeGoogleMaps() {
        if (window.innerWidth >= 752) {
                var infoContentHeight = $('#info__content').parent().height();
                $('#google-maps').css('height', infoContentHeight);
                this.updateGoogleMaps();

        } else {
            $('#google-maps').css('height', 200);
        }
    },

    render() {
        return <div className="home-content">
            <div className="container-fluid info__wrapper">
                <div className="row">
                    <div id="location" className="container info">

                        <h2>Location & Contact</h2>

                        <div className="row primary-box-shadowing">

                            <div className="col-md-6">
                                <div id="google-maps" class="google-maps"></div>
                            </div>

                            <div className="col-md-6">
                                <div id="info__content" className="info__content">
                                    <h3>Location</h3>
                                    <p>Maliebaan 68<br/>
                                        3581 CV Utrecht<br/>
                                        The Netherlands</p>
                                    <h3>Contact Us</h3>
                                    <p>If you have questions or comments please send an email to&nbsp;<a
                                        href="mailto:info@fxmedia.nl">info@fxmedia.nl</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

};