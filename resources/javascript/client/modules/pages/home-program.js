var _ = require('lodash');

var swiperSettings = {};

var mySwiper,
    lastSlide,
    lastSlideIndex,
    swiperOn = false,
    $swiperNextButton,
    $swiperPrevButton,
    initialSlideIndex = 0;

/* swiperItemsData. This data is used for initially rendering swiper slides */
var swiperSlidesData = [
    [
        {
            image: 'images/_default_/program/reception.png',
            time: '10:00',
            title: 'Day 1 Presentation',
            subtitle: 'Great one',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/break.png',
            time: '11:00',
            title: 'Another Day 1 presentation',
            subtitle: 'Great one'
        },
        {
            image: 'images/_default_/program/break.png',
            time: '12:00',
            title: 'Another Day 11 presentation',
            subtitle: 'Great one'
        },
        {
            image: 'images/_default_/program/break.png',
            time: '12:00',
            title: 'Another Day 11 presentation',
            subtitle: 'Great one'
        },
        {
            image: 'images/_default_/program/break.png',
            time: '12:00',
            title: 'Another Day 11 presentation',
            subtitle: 'Great one'
        }
    ],
    [
        {
            image: 'images/_default_/program/reception.png',
            time: '09:00',
            title: 'Day 2 Cool Presentation',
            subtitle: 'Great one'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '13:00',
            title: 'Cooool Day 2 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '09:00',
            title: 'Day 2 Cool Presentation',
            subtitle: 'Great one'
        },
    ],
    [
        {
            image: 'images/_default_/program/reception.png',
            time: '08:00',
            title: 'Day 3 Yay Presentation',
            subtitle: 'Thanks'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '19:00',
            title: 'Cooool Day 3 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '19:00',
            title: 'Cooool Day 3 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '19:00',
            title: 'Cooool Day 3 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '19:00',
            title: 'Cooool Day 3 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        },
        {
            image: 'images/_default_/program/reception.png',
            time: '19:00',
            title: 'Cooool Day 3 Presentation',
            subtitle: 'Great oneeeee',
            modal: 'program-default'
        }
    ]
];

/* Using these arrays to handle the toggling between program days */
var renderedSlides = [];
var reservedSlides = [];

/* Initialize Swiper.js 2.7.6. Using old version for old browser support. API: https://github.com/nolimits4web/Swiper/blob/Swiper2/API.md */

module.exports = {

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    componentDidMount() {

        $swiperNextButton = $('#swiper-next');
        $swiperPrevButton = $('#swiper-prev');
        
        initialSlideIndex = Math.floor($('.swiper-slide--day0').length * .5);

        swiperSettings = {
            mode: 'horizontal',
            slidesPerView: 'auto',
            roundLengths: true,
            cssWidthAndHeight: 'height',
            initialSlide: initialSlideIndex,
            offsetPxBefore: 0,
            offsetPxAfter: 0,
            centeredSlides: true,
            onTouchEnd: () => {
                this.toggleNextSwiperButton();
                this.togglePrevSwiperButton();
            },
            watchActiveIndex: true
        };

        if ($(window).width() >= 752) {

            this.resetSwiper();

            swiperOn = true;

        } else {
            swiperOn = false;
            this.collapseProgram();
        }

        $swiperNextButton.click(() => {
            mySwiper.swipeNext();
            this.toggleNextSwiperButton();
            $('#swiper-prev').removeClass('inactive');
        });

        $swiperPrevButton.click(() => {
            mySwiper.swipePrev();
            this.togglePrevSwiperButton();
            $swiperNextButton.removeClass('inactive');
        });

        $(window).resize(() => {
            this.toggleSwiper();
        });
    },

    togglePrevSwiperButton() {

        if (mySwiper.activeIndex <= 2 || mySwiper.slides.length <= 3) {
            $swiperPrevButton.addClass('inactive');

        } else {
            $swiperPrevButton.removeClass('inactive');
        }
    },

    /**
     *  If at the end of the swiper, hide the $swiperNextButton.
     *  mySwiper.activeIndex is the current 'drag' index.
     *  I do minus 3 because that's how you get the last possible 'drag' in the swiper.
     *  */
    toggleNextSwiperButton()  {
        if ((lastSlideIndex - 2 <= mySwiper.activeIndex) || mySwiper.slides.length <= 3) {
            $swiperNextButton.addClass('inactive');
        } else {
            $swiperNextButton.removeClass('inactive');
        }
    },

    collapseProgram() {
        $('.swiper-slide').slice(3).addClass('hide');
    },

    expandProgram() {
        $('.swiper-slide--day0').removeClass('hide');
        $('#programs__btn').addClass('hide');
    },

    showProgram() {
        $('.swiper-slide').removeClass('hide');
    },

    toggleSwiper() {
        if ($('.swiper-container').length === 0) {
            return false;
        }

        if ($(window).width() < 752 && typeof mySwiper !== 'undefined' && swiperOn) {

            mySwiper.destroy();

            $('.swiper-wrapper').removeAttr("style");

            swiperOn = false;

            this.collapseProgram();


        } else if ($(window).width() >= 752 && !swiperOn) {
            this.resetSwiper();
            this.showProgram();

            swiperOn = true;
        }
    },

    resetSwiper() {
        let $swiperContainers = $('.swiper-container');

        if ($swiperContainers.length === 0) { return; }

        mySwiper = $swiperContainers.swiper(swiperSettings);

        /* Reserve slide instances for toggling days */
        for (let i = 0; i < mySwiper.slides.length; i++) {
            reservedSlides.push(mySwiper.getSlide(i));
        }

        this.showProgramItemsOfDay(0);

        lastSlide = mySwiper.getLastSlide();
        lastSlideIndex = lastSlide.index();

        console.log('lastSlideIndex: ', lastSlideIndex);

        this.toggleNextSwiperButton();
    },

    handleDayButtonClick(day) {
        $('.button--day').addClass('button--day--inverted');
        $('#button--day' + day).removeClass('button--day--inverted');

        if ($(window).width() >= 752) {
            this.showProgramItemsOfDay(day);
            this.togglePrevSwiperButton();
            this.toggleNextSwiperButton();
        } else {
            $('.swiper-slide').addClass('hide');
            $('.swiper-slide--day'+day).removeClass('hide');

            $('#programs__btn').addClass('hide');
        }
    },


    showProgramItemsOfDay(day) {
        /* Moving the day's slides to the end of the swiper by appending */
        for (let i = 0; i < renderedSlides.length; i++) {

            if (renderedSlides[i].day == day) {
                mySwiper.appendSlide(reservedSlides[i]);
            }
        }

        /* Then remove the rest of the slides. Do this by removing the first slide X amount of times. X is the total number of slides minus the day's total number of slides */
        var totalToRemove = (mySwiper.hasOwnProperty('slides')) ? mySwiper.slides.length - swiperSlidesData[day].length : 0;

        for (let i = 0; i < totalToRemove; i++) {
            mySwiper.removeSlide(0);
        }

        /* Swiping back to the start of the swiper so it doesn't look weird after removing slides */
        let swipeToIndex = (mySwiper.slides.length <= 5) ? Math.floor(mySwiper.slides.length * .5) : 2;
        mySwiper.swipeTo(swipeToIndex);

        lastSlide = mySwiper.getLastSlide();
        lastSlideIndex = lastSlide.index();
    },

    renderProgramItem (data, day) {

        data.day = day;
        renderedSlides.push(data);

        return this.props.getComponent('program-item', data);
    },

    shouldComponentUpdate() {
        return false;
    },

    render() {
        return <div className="home-content">
            <div className="container-fluid programs__wrapper">
                <div className="row">
                    <div id="program" className="container programs">

                        <h2>Program</h2>

                        <div className="button--days text-center">

                            <div className="button--days__inner primary-box-shadowing">

                                <div id="button--day0" className="button button--day"
                                     onClick={this.handleDayButtonClick.bind(this, '0')}>Day
                                    1
                                </div>
                                <div id="button--day1" className="button button--day button--day--inverted"
                                     onClick={this.handleDayButtonClick.bind(this, '1')}>Day 2
                                </div>
                                <div id="button--day2" className="button button--day button--day--inverted"
                                     onClick={this.handleDayButtonClick.bind(this, '2')}>Day 3
                                </div>

                            </div>

                        </div>

                        <div id="programs__swiper" className="programs__list swiper-container">
                            <div id="swiper-prev" className="button--swiper button--swiper--prev inactive"></div>
                            <div className="swiper-wrapper">

                                { _.map(swiperSlidesData[0], (data) => this.renderProgramItem(data, 0)) }
                                { _.map(swiperSlidesData[1], (data) => this.renderProgramItem(data, 1)) }
                                { _.map(swiperSlidesData[2], (data) => this.renderProgramItem(data, 2)) }

                            </div>
                            <div id="swiper-next" className="button--swiper button--swiper--next"></div>

                        </div>

                        <div id="programs__btn" className="button button--primary programs__btn"
                             onClick={this.expandProgram}>See full program
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }

};