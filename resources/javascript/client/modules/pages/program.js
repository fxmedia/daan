module.exports = {

    getInitialState() {
        return {};
    },

    componentWillMount() {

    },

    render() {
        return <div className="page__container">

            {this.props.getComponent('pages-header', {title: {title: this.props.t('pages_header_program')}})}

            <div className="page__content page__content--padded">

                {this.props.getComponent('program-list')}

            </div>
        </div>
    }

};