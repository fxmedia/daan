var _ = require('lodash');

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function() {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState() {
        return {}
    },

    componentWillMount() {

    },

    renderPicture() {
        var avatarType = "image image__default";
        var style = {};

        if ( this.state.client.user.image !== '' ) {
            avatarType = "image";

            style = {
                backgroundImage: 'url(' + this.state.client.user.image + ')'
            };
        }

        return <div className="person__image">
            <div className="image-wrapper with-border-and-shadow">
                <div className={ avatarType } style={ style }></div>
            </div>
        </div>
    },

    render() {
        return <div className="home-content">
            <div className="container-fluid phonebook__wrapper">
                <div className="row">
                    <div id="phonebook" className="container phonebook">

                        <div className="row">

                            <div className="col-md-6 col-md-6--right-float">
                                <div className="phonebook__intro">
                                    <h2>{this.props.t('home_header_phonebook')}</h2>
                                    <p>
                                        {this.props.t('home_phonebook_body')}
                                    </p>
                                    <div onClick={this.props.state.setPhonebookModalOpen.bind(null, true)}
                                         className="button button--primary">{this.props.t('home_phonebook_btn')}
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6">
                                <div className="person">
                                    <div className="person__header">
                                        <img className="white-gradient-overlay" src="/images/_default_/gradient-square.png" />

                                        { this.renderPicture() }
                                        <h3>{ this.state.client.user.firstname } { this.state.client.user.lastname } (You)</h3>
                                        <p>{this.state.client.user.function || ''}</p>
                                        <p>{this.state.client.user.opco || ''}</p>
                                    </div>
                                    <div className="person__body">
                                        <div className="person__body__section">
                                            <h3>{this.props.t('person_phonenumber_label')}</h3>
                                            <p>{ this.state.client.user.phonenumber }</p>
                                        </div>
                                        <div className="person__body__section">
                                            <h3>{this.props.t('person_email_label')}</h3>
                                            <p>{ this.state.client.user.email }</p>
                                        </div>
                                    </div>
                                    <div className="person__footer">
                                        Are your details incorrect? Please email us at <a
                                        href="mailto:info@fxmedia.nl">info@fxmedia.nl</a>
                                    </div>
                                </div>
                            </div>



                        </div>

                    </div>
                </div>
            </div>
        </div>
    }

};