var _ = require('lodash');

module.exports = {

    getNonStandAlone() {
        var items = [];
        modules = _.sortByOrder(modules, ['order']);
        _.each(modules, (module) => {
            if (!module.is_standalone && this.props.checkStage(module) && this.props.isUserAllowed(module)) {
                items.push(module.path)
            }
        });
        
        return items;
    },

    render: function () {
        return <div className="home">
            
            {_.map(this.getNonStandAlone(), function (path) {
                return this.props.getPage('pages/' + path.replace('.js', ''), this.props.state);
            }.bind(this))}

        </div>
    }

};