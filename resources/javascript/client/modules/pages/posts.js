var _ = require('lodash');

module.exports = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Client")
    ],

    getStateFromFlux: function() {
        return {
            client: this.getFlux().store("Client").getState()
        };
    },

    getInitialState() {
        return {
            viewing: (this.props.state.data.viewing) ? this.props.state.data.viewing : 'comments'
        }
    },

    componentWillMount() {

    },

    renderUserPicture: function(user) {
        var avatarType = "image image__default";
        var style = {};

        if ( user.image !== '' ) {
            avatarType = "image";

            style = {
                backgroundImage: 'url(' + user.image + ')'
            };
        }

        return <div className="image-wrapper">
            <div className={avatarType} style={style}></div>
        </div>
    },

    renderUserInfo: function (user) {
        return <div className="message__thumb">
            { this.renderUserPicture( user) }
            <div className="name">{user.firstname} {user.lastname}</div>
        </div>
    },

    replaceURLWithHTMLLinks(text) {
        var exp = /(\b(https?|www|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
        return text.replace(exp,"<a href='$1' target='_blank'>$1</a>");
    },

    renderMessage: function (post) {
        var image = this.getImage(post);
        var message = post.message ? <p>{this.replaceURLWithHTMLLinks(post.message)}</p> : false;

        // timestamp is used in isotope for sorting - post-id un-used
        return <div data-post-id={post.id} data-timestamp={post.timestamp} className="message__list__item">
            {this.renderUserInfo(post.user)}
            <div className="message__body">
                <div className="message__body__text" dangerouslySetInnerHTML={{__html: this.replaceURLWithHTMLLinks(post.message)}} />
                {image}
            </div>
            <div className="l-clear"></div>
        </div>
    },

    getImage: function (post) {
        // objects contains movie, thumb and ani-gif
        if( post ) {
            if (typeof(post.file) == 'object') {
                if(post.file) {
                    return <img src={post.file.gif} alt="Movie" />
                }
            }
        }

        if (post.file_path) {
            return <img src={post.file_path} alt="Picture" />
        }

        if(post.file) {
            return <img src={post.file} alt="Animated GIF" />
        }

        return false;
    },

    showMessages() {
        this.setState({viewing: 'comments'});
    },

    showQuestions() {
        this.setState({viewing: 'questions'});
    },

    renderMessageChoice(questions, comments) {
        if(!questions.length || !comments.length) {
            return null;
        }

        var activeMessage = this.state.viewing === 'comments' ? ' active' : '';
        var activeQuestions = this.state.viewing === 'questions' ? ' active' : '';

        return <div className="message__choice">
            <div onClick={this.showMessages} className={"button block" + activeMessage}>Messages</div>
            <div onClick={this.showQuestions} className={"button block" + activeQuestions}>Questions</div>
            <div className="clearfix"></div>
        </div>
    },

    render() {
        var questions = _.filter(this.state.client.posts, {type: 'question'});
        var comments  = _.filter(this.state.client.posts, function(post) {
            return (!post.type || post.type == 'comment' || post.type == 'photo')
        });


        return <div className="page__container">
            {this.props.getComponent('pages-header', {title: {title: this.props.t('pages_header_posts')}})}
            <div className="page__content">
                {this.renderMessageChoice(questions, comments)}
                {function(){
                    if(this.state.viewing === 'questions') {
                        return _.map(questions, this.renderMessage);
                    }
                    return _.map(comments, this.renderMessage);
                }.bind(this)()}
            </div>
        </div>

    }

};