var Connection = require('./../../ClientConnection');

var quiz_welcome = {
    mixins: [
        Fluxxor.FluxMixin(React),
        Fluxxor.StoreWatchMixin("Quiz")
    ], 

    getStateFromFlux: function () {
        var Flux = this.getFlux();
        return {
            quiz: Flux.store('Quiz').getState()
        };
    },

    getInitialState: function () {
        return {
            info_message: '',
            completed: false,
            name: '',
            number: ''
        };
    },

    submit: function (e) {
        var name = document.getElementById('quiz_name').value;
        var number = Number(document.getElementById('quiz_number').value);

        if(!name || !number || Math.floor(number) != number) {
            this.setState({info_message: 'Vul je naam en team nummer in!'});
            return;
        }

        Connection.submitName(name, number, function(result){
            if(result.success) {
                this.setState({
                    info_message: 'Bedankt! Over een moment gaat de quiz van start.',
                    completed: true,
                    name: name,
                    number: number
                });
            } else {
                this.setState({info_message: result.message});
            }
        }.bind(this));
    },

    renderInput: function () {
        if(!this.state.completed) {
            return <div>
                <ol className="list__signup">
                    <li><input type="text" id="quiz_number" placeholder="Groepnummer" /></li>
                    <li><input type="text" id="quiz_name" placeholder="Teamnaam"/></li>
                </ol>
                <div>{this.state.info_message}</div>
                <a href="#" onClick={this.submit} className="button button--white">Start</a>
            </div>
        }

        return <div>{this.state.info_message}</div>
    },

    render: function () {
        return <div className="l-wrapper">
            <img src="images/fx-logo.png" alt="Welkom" />
            <h1>Welkom bij de <span>QUIZ</span></h1>

            {this.renderInput()}
        </div>
    }
};

module.exports = quiz_welcome;