var _   = require('lodash');

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.posts = [];
        this.active_page = 'wall/wall';
        this.wall = {};
        this.limit = 200;
        this.settings = {};

        this.bindActions(
            'addPost', this.addPost,
            'editPost', this.editPost,
            'deletePost', this.deletePost,
            'addPosts', this.addPosts,
            'setPage', this.setPage,
            'wallInfo', this.wallInfo,
            'addMorePosts', this.addMorePosts,
            'setSettings', this.setSettings
        );
    },

    sort: function () {
        this.posts = _.sortByOrder(this.posts, 'timestamp', false);
    },

    addPost: function (data) {
        this.posts.push(data);
        this.posts.forEach(function(post, key){
            if(post.user.id == data.user.id){
                this.posts[key].user.image = data.user.image;
            }
        }.bind(this));
        this.sort();
        this.checkLimit();

        this.emit('change');
    },

    editPost: function (post) {
        _.forEach(this.posts, function(_post, k){
            if(post.id == _post.id) {
                this.posts[k] = post;
            }
        }.bind(this));
        this.sort();
        this.emit('change');
    },

    deletePost: function (post) {
        var key = null;
        
        _.forEach(this.posts, function(_post, k){
            if(post.id == _post.id) {
                key = k;
            }
        });
        if(key !== null) {
            this.posts.splice(key, 1);
        }

        this.emit('change');
    },

    addPosts: function(data) {
        this.posts = [];
        _.forEach(data, function(post){
            this.posts.push(post);
        }.bind(this));
        this.sort();
        this.checkLimit();
        this.emit('change');
    },

    addMorePosts: function(data) {
        _.forEach(data, function(post){
            this.posts.push(post);
        }.bind(this));
        this.sort();
        this.emit('change');
    },

    setPage: function(page) {
        this.active_page = page;
        this.emit('change');
    },

    wallInfo: function(data) {
        this.wall = data;
        this.emit('change');
    },

    checkLimit: function () {
        if(this.posts.length > this.limit) {
            while (this.posts.length > this.limit) {
                try {
                    this.posts.splice(this.posts.length - 1, 1);
                } catch (e) {
                    
                }
            }
        }
    },

    setSettings: function( settings ) {
        this.settings = settings;
        this.emit('change');
    },

    getState: function () {
        return {
            posts: this.posts,
            active_page: this.active_page,
            wall: this.wall,
            settings: this.settings
        };
    }

});