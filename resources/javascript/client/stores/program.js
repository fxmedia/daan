var _ = require('lodash');

var days = [
    "20170508",
    "20170509",
    "20170510"
];

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.program = [];
        this.active = -1;

        this.bindActions(
            'setProgram', this.setProgram,
            'setActive', this.setActive
        );
    },

    setProgram: function(data){

        this.program = data.map(function(item){
            let sArr = item.starting_time.split(':');
            item.start = (sArr.length)? sArr[0] + ':' + sArr[1] : "00:00";

            if(item.ending_time) {
                let eArr = item.ending_time.split(':');
                item.end = (eArr.length) ? eArr[ 0 ] + ':' + eArr[ 1 ] : "00:00";
            }


            item.day = days.indexOf(item.date_string);

            if(item.date_string) {
                let dateArr = [item.date_string.substr(0, 4), item.date_string.substr(4, 2), item.date_string.substr(6, 2)];

                if(sArr.length) item.date = new Date(dateArr[0], dateArr[1] - 1, dateArr[2], sArr[0], sArr[1]);
                else item.date = new Date(dateArr[0], dateArr[1] - 1, dateArr[2]);
            } else item.date = false;

            return item;

        }).sort(function(a,b){
            if(a.date < b.date) return -1;
            if(a.date > b.date) return 1;
            return 0;
        });
        
        this.emit('change');
    },
    setActive: function(id){
        this.active = id;
        this.emit('change');
    },

    getState: function () {
        return {
            program: this.program,
            active: this.active
        };
    }

});