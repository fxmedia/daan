var _ = require('lodash');

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.active_page = 'wall/wall';
        this.sub_page = 'home';
        this.wall_stage = stage;
        this.wall = {};
        this.user = {};
        this.push_content = {};
        this.settings = {};
        this.votingUsers = [];
        this.posts = [];
        this.post_limit = 200;

        this.bindActions(
            'setPage', this.setPage,
            'wallInfo', this.wallInfo,
            'setUser', this.setUser,
            'setPushContent', this.setPushContent,
            'setSettings', this.setSettings,
            'startVoting', this.startVoting,
            'setSubPage', this.setSubPage,
            'setStage', this.setStage,
            'addPost', this.addPost,
            'deletePost', this.deletePost,
            'addPosts', this.addPosts,
        );
    },

    setPage: function (page) {
        this.active_page = page;
        this.emit('change');
    },

    wallInfo: function (data) {
        this.wall = data;
        this.emit('change');
    },

    setUser: function(data) {
        this.user = data;
        this.emit('change');
    },

    userPicture: function (path) {
        
        this.user.image = path;
        this.emit('change');
    },

    setPushContent: function(data) {
        this.push_content = data.content;
    },

    setSettings: function(settings) {
        this.settings = settings;
        this.emit('change');
    },
    
    startVoting: function(users) {
        this.votingUsers = users;
        this.emit('change');
    },

    setSubPage: function(page) {
        this.sub_page = page;
        this.emit('change');
    },

    setStage(stage) {
        this.wall_stage = parseInt(stage);
        this.emit('change');
    },

    sort: function () {
        this.posts = _.sortByOrder(this.posts, 'timestamp', false);
    },

    addPost: function (data) {
        this.posts.push(data);
        this.sort();
        this.checkLimit();

        this.emit('change');
    },

    deletePost: function (post) {
        var key = null;
        
        _.forEach(this.posts, function(_post, k){
            if(post.id == _post.id) {
                key = k;
            }
        });
        if(key !== null) {
            this.posts.splice(key, 1);
        }

        this.emit('change');
    },

    addPosts: function(data) {
        this.posts = [];
        _.forEach(data, function(post){
            this.posts.push(post);
        }.bind(this));
        this.sort();
        this.checkLimit();
        this.emit('change');
    },

    checkLimit: function () {
        if(this.posts.length > this.post_limit) {
            while (this.posts.length > this.post_limit) {
                try {
                    this.posts.splice(this.posts.length - 1, 1);
                } catch (e) {
                    
                }
            }
        }
    },

    getState: function () {
        return {
            active_page: this.active_page,
            sub_page: this.sub_page,
            wall: this.wall,
            user: this.user,
            push_content: this.push_content,
            settings: this.settings,
            voting: this.votingUsers,
            stage: this.wall_stage,
            posts: this.posts
        };
    }

});