var _ = require('lodash');

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.questionnaires = [];
        this.answers = [];
        this.active = 0;

        this.bindActions(
            'startQuestionnaire', this.startQuestionnaire,
            'setQuestionnaireAnswers', this.setQuestionnaireAnswers,
            'emptyQuestionnaires', this.empty,
            'endQuestionnaire', this.endQuestionnaire,
        );
    },

    startQuestionnaire: function( questionnaire ) {
        

        this.questionnaires.push(questionnaire);
        this.emit('change');
    },

    setQuestionnaireAnswers: function(answers) {
        this.answers = answers;
        this.emit('change');
    },

    empty: function() {
        this.questionnaires = [];
        this.active = 0;
        this.emit('change');
    },

    endQuestionnaire: function(questionnaire) {
        var k = false;
        _.forEach(this.questionnaires, function(q, key) {
            if(questionnaire.id == q.id) {
                k = key;
            }
        });

        if(false !== k) {
            _.forEach(this.questionnaires[k].questions, function(question) {
                 if( this.answers.indexOf(question.id) > -1 ) {
                     this.answers.splice(this.answers.indexOf(question.id), 1);
                 }
            }.bind(this));

            this.questionnaires.splice(k, 1);
        }
    },

    setActive: function(active_id) {
        _.forEach(this.questionnaires, function(q, key) {
            if(q.id == active_id) {
                this.active = key;
                return false;
            }
        }.bind(this));
        this.emit('change');
    },

    setAnswer: function (answer) {
        

        this.answers.push(answer);
        this.emit('change');
    },

    getState: function () {
        return {
             questionnaires: this.questionnaires,
             questionnaire: this.questionnaires[this.active] || {},
             answers: this.answers
        };
    }

});