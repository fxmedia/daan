var _   = require('lodash');

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.oqAnswers = [];
        this.oqQuestion = {question: 'Een goede open vraag is kort en eenvoudig. De vraag bestaat uit één zin. Wanneer start je met project X?'};
        this.highlightAnswer = false;

        this.bindActions(
            'oqAddAnswer', this.oqAddAnswer,
            'oqAddAnswers', this.oqAddAnswers,
            'oqSetQuestion', this.oqSetQuestion,
            'oqStop', this.oqStop,
            'oqHighlightAnswer', this.oqHighlightAnswer,
            'oqRemoveHighlight', this.oqRemoveHighlight,
            'setPage', this.setPage
        );
    },

    oqAddAnswer: function (data) {
        this.oqAnswers.push(data);

        this.emit('change');
    },

    oqHighlightAnswer: function (answer) {
        this.highlightAnswer = answer;

        this.emit('change');
        this.emit('highlight');
    },

    oqRemoveHighlight: function (answer) {
        this.highlightAnswer = false;

        this.emit('change');
        this.emit('highlight');
    },

    oqSetQuestion: function (data) {
        this.oqQuestion = data.question;

        this.emit('change');
    },

    oqStop: function() {
        this.oqQuestion = '';
        this.oqAnswers = [];

        this.emit('change');
    },

    oqAddAnswers: function(data) {
        this.oqAnswers = [];
        _.forEach(data, function(answer){
            this.oqAnswers.push(answer);
        }.bind(this));
        this.emit('change');
    },

    setPage: function(page) {
        this.active_page = page;
        this.emit('change');
    },

    getState: function () {
        return {
            oqAnswers: this.oqAnswers,
            oqQuestion: this.oqQuestion,
            active_page: this.active_page,
            wall: this.wall,
            settings: this.settings,
            highlightAnswer: this.highlightAnswer
        };
    }

});