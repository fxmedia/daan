var _ = require('lodash'),
    delay = 50,
    TimeoutInterval = null,
    stopQuestion = false;

module.exports = Fluxxor.createStore({
    initialize: function () {
        this.active_question = null;
        this.answer = null;
        this.is_active = false;
        this.name = null;
        this.score = null;
        this.my_score = 0;
        this.my_answer = null;
        this.my_name = null;
        this._type = 'quiz';
        this.locked = false;
        this.players = 0;
        this.score_page = 1;
        this.score_type = 'normal';
        this.display_validated = false;

        this.bindActions(
            'setScore', this.setScore,
            'setQuestion', this.setQuestion,
            'validate', this.validateQuestion,
            'validateClient', this.validateAnswerClient,
            'startQuiz', this.startQuiz,
            'endQuiz', this.endQuiz,
            'setName', this.setName,
            'setAnswer', this.setAnswer,
            'setMyScore', this.setMyScore,
            'setMyName', this.setMyName,
            'setType', this.setType,
            'setPlayers', this.setPlayers,
            'setScorePage', this.setScorePage,
            'setScoreType', this.setScoreType,
            'setQuestionFallback', this.setQuestionFallback
        );
    },

    setPollType: function (type) {
        this.type = type;
        this.emit('change');
    },

    setScore: function (score) {
        this.score = Object.keys(score).map(function(k){return score[k]});
        this.emit('change');
    },

    setQuestion: function (question) {
        this.active_question = question;
        this.answer = null;
        this.my_answer = null;
        this.display_validated = false;
        this.locked = false;
        this.emit('change');
        this.emit('change_question');
    },

    setQuestionFallback: function (question) {
        if(this.active_question.id != question.id) {
            this.setQuestion(question);
            this.getFlux().actions.setPage('quiz_question');
        }

    },

    animateValidation: function( cb ) {
        //
        var items = $('.list__answer li');
            //items = _.shuffle(items);

        _.forEach(items, function( el ) {
            $(el).delay(delay).fadeOut(250).fadeIn(400);
            delay*=1.2;
        });

        if( delay < 400 ) {
            if(stopQuestion) return;

            this.animateValidation( cb );
            return;
        }

        setTimeout( cb, 2500 );
    },

    validateQuestion: function (answer) {
        //stopQuestion = false;
        //value = 50;

        //if( this._type == 'game' ) {
            this.answer = answer;
            this.display_validated = true;
            this.emit('change');
        //} else {
        //    console.log('Starting animation');
        //
        //    this.animateValidation(function () {
        //        if(stopQuestion) { return; }
        //
        //        this.answer = answer;
        //        this.emit('change');
        //    }.bind(this));
        //}
    },

    validateAnswerClient: function (answer) {
        this.locked = true;
        this.answer = answer;
        this.emit('change');
    },

    startQuiz: function (name, type) {
        this.is_active = true;
        this.name = name;
        this.display_validated = false;
        this.emit('change');
    },

    endQuiz: function () {
        this.is_active = false;
        this.display_validated = false;
        this.active_question = null;
        this.locked = false;
    },

    setName: function (name) {
        this.name = name;
    },

    setAnswer: function (answer) {
        this.answer = answer;
        this.emit('change');
    },

    setMyAnswer: function (answer) {
        this.my_answer = answer;
        this.emit('change');
    },

    setMyScore: function(points) {
        this.my_score = points;
        this.emit('change');
    },

    setMyName: function (name) {
        this.my_name = name;
        this.emit('change');
    },

    setType: function(type) {
        this._type = type;
        this.emit('change');
    },

    setPlayers: function( players ) {
        this.players = players;
        this.emit('change');
    },

    setScorePage: function( page ) {
        this.score_page = page;
        this.emit('change');
    },

    setScoreType: function( type ) {
        this.score_type = type;
    },

    getState: function () {
        return {
            active_question: this.active_question,
            is_active: this.is_active,
            name: this.name,
            score: this.score,
            my_score: this.my_score,
            answer: this.answer,
            my_answer: this.my_answer,
            my_name: this.my_name,
            type: this._type,
            locked: this.locked,
            players: this.players,
            score_page: this.score_page,
            score_type: this.score_type,
            display_validated: this.display_validated
        };
    }

});