module.exports = {


    /** Client translations **/

    // Wall
    input_to_many_characters: 'U heeft te veel karakters gebruikt',
    empty_text_and_media: 'Plaats een bericht of upload een foto',
    message_sending: 'Bericht wordt verstuurd..',
    message_sent: 'Uw bericht is geplaatst',
    placeholder_input_text: 'Typ hier uw bericht',
    dear: 'Dear',

    preevent_header_h1: 'You\'re invited to reply.live!',
    preevent_attending_header_h1: 'You are successfully signed in for reply.live!',
    preevent_waiting_header_h1: 'You are on the waiting list for reply.live!',
    preevent_header_h2: 'Subtitle of event',
    preevent_header_p: '00 Month & 00 Month 2017, Location',
    preevent_sign_up_btn_label: 'Sign up now!',
    preevent_sign_out_btn_label: 'Sign out',

    duringevent_header_h1: 'Welcome to Reply.live!',
    duringevent_header_h2: 'Lorem ipsum dolor sit amet',
    duringevent_header_p: '00 Month & 00 Month 2017, Location',

    afterevent_header_h1: 'Thank you for attending reply.live!',
    afterevent_header_h2: 'Lorem ipsum dolor sit amet.',

    circle_text_message: 'Plaats een bericht',
    circle_text_question: 'Post a question',
    upload_media_placeholder_text: '+ Voeg een foto toe',
    input_characters_left: 'karakters over',
    send_button_text: 'Verstuur',
    option_row_message: 'Bericht',
    option_row_question: 'Vraag',
    view_posts: 'Bekijk de interactieve wall',
    fileupload_file_being_uploaded: 'Even geduld.. uw bestand wordt geüpload',
    fileupload_placeholder_to_large: 'Het geselecteerde bestand is te groot',
    fileupload_placeholder_to_long: 'De geselecteerde video is te lang',
    fileupload_placeholder_something_wrong: 'Er is iets mis gegaan tijdens het uploaden',
    fileupload_placeholder_upload_successful: 'Het bestand is geüpload. Druk op de knop om je bericht te versturen.',
    fileupload_video_uploaded: 'Your video is uploaded, press the send button below to submit. It takes a few minutes before your video is visible on the wall.',

    //General Modal
    close_btn_label: 'Close',
    try_again_btn_label: 'Try again',

    //Sign up modal
    sign_up_body: 'Please fill in the form. * Required fields.',
    sign_up_successful_header: 'Thanks for registering.',
    sign_up_successful_body: 'We will contact you soon.',
    sign_up_waiting_header: 'Thanks for registering. You are currently on the waiting list.',
    sign_up_waiting_body: 'You will receive an email when you are no longer on the waiting list.',
    sign_up_btn_label: 'Sign up now',

    //Sign out modal
    sign_out_body: 'Would you like to cancel your registration?',
    sign_out_btn_label: 'Cancel registration',
    sign_out_successful_header: 'You have signed out successfully. We hope to welcome you next time!',

    // Questionnaire
    questionnaire_intro_body: 'Op uw telefoon krijgt u een paar vragen om te beantwoorden. Deze survey heeft 10 vragen en duurt ongeveer 5 minuten. Druk op de knop hieronder om te starten.',
    questionnaire_outro_body: 'Laten we met elkaar blijven praten, <br/>meld je nu aan op: <br/><a style="color:#3d7edb" href="http://tatasteelmedewerkersplatform.nl">tatasteelmedewerkersplatform.nl</a>',
    questionnaire_placeholder_input_text: 'Uw antwoord...',

    // Quiz
    display_wall_header_tagline: '',
    introduction_body: 'Vandaag is deze tablet een interactie tool, Doe mee met de Quiz, en rank de pitches. Veel plezier!',
    answer_question: 'Answer the following question',
    shootout_answer_confirm: 'Confirm Answer',

    // Hats

    // Poll

    // Push

    // Header

    // Scoreboard

    // Phonebook
    phonebook_search_placeholder: 'Type to search',

    // Program
    day1_btn_label: 'Day 1',
    day2_btn_label: 'Day 2',
    day3_btn_label: 'Day 3',

    // Register Page
    register_welcome: 'Welkom',
    register_body: 'Door u hier aan te melden kunt u deelnemen aan interactieve momenten',
    register_submit_btn: 'Bevestigen',
    register_firstname: 'Nickname',
    register_email: 'Email',

    // Posts

    label_your_score: 'Your score',
    label_position: 'Position',
    label_current_game: 'Current',
    label_total: 'Total',
    label_correct_answer: 'Correct!',
    label_wrong_answer: 'Too bad! Wrong answer',
    label_wait_question: 'Even geduld alstublieft',
    label_make_choice: 'Make your choice',
    label_players_left:  'players left',
    label_poll:  'Poll',
    confirm_decline: 'Annuleren',
    confirm_confirm: 'Stem',
    label_thanks: 'Bedankt',

    label_next_question: 'Volgende vraag',
    label_start_survey: 'Start survey',
    label_survey: 'Survey',
    label_back: 'Terug',
    label_current_score: 'Current score',
    label_final_score: 'Final score',
    label_make_a_choice: 'Make a choice',


    ad_banner_label: 'powered by',
    ad_banner_header: 'Reply.live, the digital platform for your live events.',
    ad_banner_body: 'Create an ultimate experience with the interactive apps for live and realtime interaction.',
    ad_banner_link: 'More information about Reply.live',

    pages_header_phonebook: 'Phonebook',
    pages_header_about: 'About',
    pages_header_program: 'Program',
    pages_header_posts: 'Interactieve wall',
    pages_header_questionnaire: 'Questionnaire',
    pages_header_strategy: 'Strategy',

    person_phonenumber_label: 'Phone number',
    person_email_label: 'Email address',
    person_see_yourself_btn_label: 'Click here to see your profile',

    home_header_phonebook: 'Phonebook',
    home_phonebook_body: 'Check out who of your colleagues will attend the event.',
    home_phonebook_btn: 'Show all colleagues',
    home_header_program: 'Program',
    home_program_btn: 'See entire program',

    view_wall_btn_label: 'Bekijk de interactieve wall',

    /** Display translations **/
    questions_header: 'Questions',
    messages_header: 'Messages',
};

