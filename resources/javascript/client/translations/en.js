module.exports = {


    /** Client translations **/

    // Wall
    input_to_many_characters: 'You have used too many characters',
    empty_text_and_media: 'Please enter a message or upload media',
    message_sending: 'Sending message ..',
    message_sent: 'Your message has been posted successfully',
    placeholder_input_text: 'Add text',
    dear: 'Dear',

    event_name: 'Reply.live',

    preevent_header_h1: 'you\'re invited to join',
    preevent_attending_header_h1: 'You signed up for eventname',
    preevent_waiting_header_h1: 'You are on the waiting list for reply.live!',
    preevent_header_h2: 'Subtitle of event',
    preevent_header_p: '00 Month & 00 Month 2017, Location',
    preevent_sign_up_btn_label: 'Sign up now!',
    preevent_sign_out: 'If you can’t come, cancel your entry ',

    duringevent_header_h1: 'Welcome to Reply.live!',
    duringevent_header_h2: 'Lorem ipsum dolor sit amet',
    duringevent_header_p: '00 Month & 00 Month 2017, Location',

    afterevent_header_h1: 'Thank you for attending reply.live!',
    afterevent_header_h2: 'Lorem ipsum dolor sit amet.',

    circle_text_message: 'Post a message',
    circle_text_question: 'Post a question',
    circle_text_discuss_header: 'Discuss',
    circle_text_discuss_body: 'Post and view messages',
    upload_media_placeholder_text: '+ ADD PHOTO OR VIDEO',
    input_characters_left: 'characters left',
    send_button_text: 'Send',
    option_row_message: 'Message',
    option_row_question: 'Question',
    view_posts: 'View the interactive wall',
    post_submitted: 'Your post has successfully been submitted.',
    fileupload_file_being_uploaded: 'Please wait.. your file is being uploaded',
    fileupload_placeholder_to_large: 'The selected file is too large',
    fileupload_placeholder_to_long: 'The selected video is too long',
    fileupload_placeholder_something_wrong: 'Something went wrong while uploading',
    fileupload_placeholder_upload_successful: 'The file is uploaded. Press send to submit your file.',
    fileupload_video_uploaded: 'Your video is uploaded, press the send button below to submit. It takes a few minutes before your video is visible on the wall.',

    //General Modal
    close_btn_label: 'Close',
    try_again_btn_label: 'Try again',

    //Sign up modal
    sign_up_body: 'We already have some information but we still need some info. Please fill in the open fields.',
    sign_up_successful_header: 'Thanks for registering.',
    sign_up_successful_body: 'We will contact you soon.',
    sign_up_waiting_header: 'Thanks for registering. You are currently on the waiting list.',
    sign_up_waiting_body: 'You will receive an email when you are no longer on the waiting list.',
    sign_up_btn_label: 'Sign up now',

    //Sign out modal
    sign_out_body: 'Would you like to cancel your registration?',
    sign_out_btn_label: 'Cancel registration',
    sign_out_successful_header: 'You have signed out successfully. We hope to welcome you next time!',

    // Questionnaire
    questionnaire_intro_body: 'You’ll get a couple of questions to answer on your own phone. This survey has 10 question and will take 5 minutes of your time. Please tap the button below to start',
    questionnaire_outro_body: 'What will happen with these answers... dictum, diam et volutpat convallis, urna dolor eleifend nisi, vitae rutrum sapien eros at nisi. Duis rhoncus dui tortor, nec malesuada purus sodales non. Mauris at dignissim sapien, eu elementum nibh.',
    questionnaire_placeholder_input_text: 'Your answer...',

    // Quiz
    display_wall_header_tagline: '',
    introduction_body: 'Vandaag is deze tablet een interactie tool, Doe mee met de Quiz, en rank de pitches. Veel plezier!',
    answer_question: 'Answer the following question',
    shootout_answer_confirm: 'Confirm Answer',

    // Hats

    // Poll

    // Push

    // Header

    // Scoreboard

    // Phonebook
    phonebook_search_placeholder: 'Type to search',

    // Program
    day1_btn_label: 'Day 1',
    day2_btn_label: 'Day 2',
    day3_btn_label: 'Day 3',

    // Register Page
    register_welcome: 'Welcome',
    register_body: 'Please register and participate during interactive moments.',
    register_submit_btn: 'Submit',
    register_firstname: 'Firstname',
    register_email: 'Email',

    // Posts

    label_your_score: 'Your score',
    label_position: 'Position',
    label_current_game: 'Current',
    label_total: 'Total',
    label_correct_answer: 'Correct!',
    label_wrong_answer: 'Too bad! Wrong answer',
    label_wait_question: 'One moment please',
    label_make_choice: 'Make your choice',
    label_players_left:  'players left',
    label_poll:  'Poll',
    confirm_decline: 'Cancel',
    confirm_confirm: 'Submit',
    label_thanks: 'Thanks',

    label_next_question: 'Next Question',
    label_start_survey: 'Start survey',
    label_survey: 'Survey',
    label_back: 'Back',
    label_current_score: 'Current score',
    label_final_score: 'Final score',
    label_make_a_choice: 'Make a choice',


    ad_banner_label: 'powered by',
    ad_banner_header: 'Reply.live, the digital platform for your live events.',
    ad_banner_body: 'Create an ultimate experience with the interactive apps for live and realtime interaction.',
    ad_banner_link: 'More information about Reply.live',

    pages_header_phonebook: 'Phonebook',
    pages_header_about: 'About',
    pages_header_program: 'Program',
    pages_header_posts: 'Interactive wall',
    pages_header_questionnaire: 'Questionnaire',
    pages_header_strategy: 'Strategy',

    person_phonenumber_label: 'Phone number',
    person_email_label: 'Email address',
    person_see_yourself_btn_label: 'Click here to see your profile',

    home_header_phonebook: 'Phonebook',
    home_phonebook_body: 'Check out who of your colleagues will attend the event.',
    home_phonebook_btn: 'Show all colleagues',
    home_header_program: 'Program',
    home_program_btn: 'See entire program',

    view_wall_btn_label: 'View the interactive wall',

    /** Display translations **/
    questions_header: 'Questions',
    messages_header: 'Messages',
};

