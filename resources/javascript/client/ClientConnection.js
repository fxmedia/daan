var Connection = require('./sockjs-wrap/client'),
    _   = require('lodash');

var Controller = function(){
    this.reconnect_info = {
        timer: null,
        short_tries: 5,
        short_timeout: 1000,
        long_timeout: 5 * 1000,
        short_counter: 0
    };

    this.connected = false;
    this.pendingPosts = [];
    this.quizData = {};
    this._connection = Connection;
};

Controller.prototype.init = function () {

    Connection.on('connect', function(){
         
        this.connected = true;

        if(token === '0') {
            return Connection.authenticate('anonymous_user');
        }
        Connection.authenticate(token);
    }.bind(this));

    Connection.on('authenticated', function(result){
         
        if(result.result) {
            flux.actions.setSettings(result.settings);

            var posts = [];
            Connection.send('load_more', 0, function (r) {
                if (r && typeof r == "object") {
                    posts = posts.concat(r)
                }
            });
            Connection.send('load_more', 100, function (r) {
                if (r && typeof r == "object") {
                    posts = posts.concat(r)
                }
            });
            Connection.send('load_more', 200, function (r) {
                if (r && typeof r == "object") {
                    posts = posts.concat(r)
                }
            });
            Connection.send('load_more', 300, function (r) {
                if (r && typeof r == "object") {
                    posts = posts.concat(r)
                }
            });
            Connection.send('load_more', 400, function (r) {
                if (r && typeof r == "object") {
                    posts = posts.concat(r)
                }
            });
            setTimeout(function () {
                
                flux.actions.addPosts(posts);
            }, 1000);

            if(this.pendingPosts.length > 0) {
                this.pendingPosts.map(function(post){
                    this.wallPost(post, null);
                }.bind(this));
                this.pendingPosts = [];
            }
            if(this.quizData.name) {
                this.submitName(this.quizData.name);
                this.quizData = {};
            }
        }
    }.bind(this));

    Connection.on('close', function(){
        
        this.connected = false;

        var timeout = 0;
        if(!this.reconnect_info.short_tries || this.reconnect_info.short_counter < this.reconnect_info.short_tries) {
            timeout = this.reconnect_info.short_timeout;
            this.reconnect_info.short_counter ++;
        } else {
            timeout = this.reconnect_info.long_timeout;
            this.reconnect_info.short_counter = 1;
        }

        this.reconnect_info.timer = setTimeout(function(){
            

            this.restart();

        }.bind(this), timeout);
    }.bind(this));

    Connection.on('show_wall', function(data){
        // flux.actions.addPosts(data.posts);
        flux.actions.wallInfo(data.wall);
        flux.actions.setUser(data.user);
        flux.actions.setPage('general/home');
    });

    Connection.on('show_quiz', function(data){
        flux.actions.setQuestion(data.question);
        flux.actions.setName(data.quiz);
        flux.actions.setAnswer(data.answer);
        flux.actions.setMyName(data.name);
        flux.actions.setUser(data.user);

        if(data.question) {
            flux.actions.setPage('quiz/quiz_question');
        } else {
            flux.actions.setPage('general/waiting');
        }
    });

    Connection.on('show_results', function(data){
        flux.actions.setUser(data.user);
        //flux.actions.setScore(data.score);
        flux.actions.setPage('wall/quiz_end');
    });

    Connection.on('start_quiz', function(data){
        flux.actions.setUser(data.user);
        flux.actions.startQuiz(data.quiz);
        flux.actions.setPage('general/waiting');
    });

    Connection.on('end_quiz', function(data){
        flux.actions.setUser(data.user);
        flux.actions.endQuiz();
        flux.actions.setPage('general/waiting');
    });

    Connection.on('set_question', function(data){
        flux.actions.setMyName(data.user.quiz_name);
        flux.actions.setUser(data.user);
        flux.actions.setQuestion(data.question);
        flux.actions.setPage('quiz/quiz_question');
    });

    Connection.on('set_question_fallback', function (data) {
        flux.actions.setQuestionFallback(data.question);
    });

    Connection.on('start_game', function(data){
        flux.actions.setUser(data.user);
        flux.actions.setType('game');
        flux.actions.setPage('general/waiting');
    });

    Connection.on('end_game', function(data){
        flux.actions.setUser(data.user);
        flux.actions.endQuiz();
        flux.actions.setPage('general/home');
    });

    Connection.on('show_game', function(data){
        flux.actions.setQuestion(data.question);
        flux.actions.setAnswer(data.answer);
        flux.actions.setUser(data.user);
        if(data.question && !data.answer) {
            flux.actions.setPage('game/game_question');
        } else {
            flux.actions.setPage('general/waiting');
        }
    });

    Connection.on('game_over', function(data){
        flux.actions.setUser(data.user);
        flux.actions.setPage('general/wrong');
    });

    Connection.on('set_game_question', function(data){
        flux.actions.setQuestion(data.question);
        flux.actions.setUser(data.user);
        flux.actions.setPage('game/game_question');
    });

    Connection.on('validate_game_question', function(data){
        flux.actions.validateQuestion(data.answer);
        flux.actions.setUser(data.user);
        var state = flux.stores.Quiz.getState();
        Connection.send('submit_game_answer', {
            answer: state.my_answer,
            question_id: state.active_question.id
        });
    });

    Connection.on('start_poll', function (data) {
        flux.actions.setUser(data.user);

        
        // flux.actions.setPage('waiting');
    });

    Connection.on('waiting', function (data) {
        flux.actions.setUser(data.user);
        flux.actions.setPage('general/waiting');
    });

    Connection.on('set_poll_question', function(question) {
        flux.actions.setUser(question.user);
        flux.actions.setQuestion(question);
        flux.actions.setPage('poll/quiz_question');
    });

    Connection.on('validate_question', function(data){
        flux.actions.setUser(data.user);
        flux.actions.validateQuestion(data.answer);
        var state = flux.stores.Quiz.getState();
        Connection.send('submit_answer', {
            answer: state.my_answer,
            question_id: state.active_question.id
        }, null);
    });

    Connection.on('push_content', function(data) {
        flux.actions.setPushContent(data);
        flux.actions.setPage('general/push_content');
    });

    Connection.on('update_user', function(user) {
        

        flux.actions.setUser(user);
    });

    //Connection.on('start_voting', function(data){
    //    console.log('start voting', data);
    //    flux.actions.startVoting(data.users);
    //    flux.actions.setPage('voting');
    //});

    /**
     * Questionnaire actions
     */
    Connection.on('start_questionnaire', function(data) {
        flux.actions.startQuestionnaire(data.questionnaire);

        if(data.questionnaire.push === "1") {
            flux.actions.setPage('questionnaire/questionnaire');
        }
    });

    Connection.on('continue_questionnaire', function(data) {

        //if (data.answers.length == data.questionnaire.questions.length) {
        //    flux.actions.setQuestionnaireAnswers([]);
        //    flux.actions.setPage('wall');
        //} else {
        flux.actions.emptyQuestionnaires();
        flux.actions.setQuestionnaireAnswers(data.answers);

        _.forEach(data.questionnaires, function(q) {
            flux.actions.startQuestionnaire(q);
        });

        if(data.questionnaires[0].push === "1") {
            if ( data.questionnaires.length === 1 ) {
                flux.actions.setPage('questionnaire/questionnaire');
            } else {
                flux.actions.setPage('questionnaire/questionnaire_start');
            }
        }
        //}
    });

    Connection.on('end_questionnaire', function(data) {
        flux.actions.endQuestionnaire(data.questionnaire);
        if(data.questionnaire.push === "1") {
            flux.actions.setPage('general/home');
        }
    });


    Connection.on('set_stage', (data) => {
        flux.actions.setStage(data.stage);
    });

    Connection.on('wall_post', function(data){
        flux.actions.addPost(data);
    });

    Connection.on('delete_post', function (post) {
        flux.actions.deletePost(post);
    });

    Connection.on('oq_stop', function(data) {
        flux.actions.oqStop();
        flux.actions.setPage('general/home');
    });

    Connection.on('oq_set_question', function(question) {
        flux.actions.oqSetQuestion(question);
        flux.actions.setPage('open_question/main');
    });

    return this;
};

Controller.prototype.start = function(){
    Connection.start({
        port: port,
        url: host
    });
};

Controller.prototype.restart = function(){
    Connection.disconnect();
    this.start();
};


Controller.prototype.wallPost = function(data, cb) {
    if(!this.connected) {
        this.pendingPosts.push(data);
        cb({success: false, message: 'Could not connect to the server right now, your message  will be updated as soon as the connection is back!'});
    } else {
        Connection.send('wall_post', data, cb);
    }
};

Controller.prototype.submitName = function(name, number, cb) {
    if(!this.connected) {
        this.quizData.name = name;
        this.quizData.number = number;
        cb({success: false, message: 'Could not connect to the server right now, your info will be updated as soon as the connection is back!'})
    } else {
        Connection.send('set_user_info', {
            name: name,
            number: number
        }, cb);
    }
};

Controller.prototype.submitQuizAnswer = function(answer, question_id) {
    
    Connection.send('submit_answer', {
        answer: answer,
        question_id: question_id
    }, null);
};

Controller.prototype.uploadUserImage = function (name, path) {
    Connection.send('change_user_image', {
        name: name,
        path: path
    }, function (d) {
        
    });
};

Controller.prototype.answerPoll = function (answer) {
    Connection.send('answer_poll', {answer: answer}, function(r) {
        
    });
};

Controller.prototype.votingAnswers = function(answers) {
    Connection.send('voting_answers', {answers: answers}, function(user){
        

        flux.actions.setUser(user);
    });
};

Controller.prototype.questionnaireAnswer = function(question_id, answer, questionnaire_id) {
    Connection.send('questionnaire_answer', {
        question_id: question_id,
        answer: answer,
        questionnaire_id: questionnaire_id
    }, function(r) {
        
    });
};

Controller.prototype.oqSendAnswer = function(answer, questionId) {
    Connection.send('oq_send_answer', {
        answer: answer,
        question_id: questionId
    }, function(r) {

    });
    setTimeout(function() {
        flux.actions.setPage('general/home');
    }, 3500);
};

Controller.prototype.getPhonebookData = function(cb) {
    Connection.send('phonebook_data', {}, function(data) {
        cb(data);
    });
};

Controller.prototype.shootoutAnswer = function(question_id, answer) {
    Connection.send('shootout_answer', {
        question_id: question_id,
        answer:      answer
    }, function(r) {
        
    });
};

module.exports = exports = new Controller();