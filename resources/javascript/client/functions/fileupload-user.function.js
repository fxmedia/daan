var Connection = require('./../ClientConnection');

module.exports = function(context, id) {
    id = id || '#user_image';

    var options = {
        url: '/wall/upload',
        dataType: 'json',
        replaceFileInput: false,
        maxFileSize: 15000000 // 15 MB
    };

    options.add = function (e, data) {
        console.log('Photo add', context.state);

        $('.progressbar-container').show();
        data.formData = {user_id: context.state.client.user.id};
        data.submit();
    }

    options.done = function (e, data) {
        setTimeout(function () {
            $('.progressbar-container').hide();
            if(window.progressbar) window.progressbar.set(0);
            else if(context.progressbar) context.progressbar.set(0);
            
            if (data.result.error) {

            } else {
                var name = data.result.filename;
                var path = data.result.filepath;

                context.getFlux().store("Client").userPicture(path);

                Connection.uploadUserImage(name, path);
            }
        }, 1000);
    };

    options.fail = function (e, data) {
        console.error('upload failed', data.errorThrown);

        $('.progressbar-container').hide();
    };

    options.progress = function (e, data) {
        if(window.progressbar) window.progressbar.animate(parseInt(data.loaded / data.total));
        else if(context.progressbar) context.progressbar.animate(parseInt(data.loaded / data.total));
    };

    $(id).fileupload(options);
}