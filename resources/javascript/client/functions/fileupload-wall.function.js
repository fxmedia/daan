module.exports = function(context) {
    $('#input_image').fileupload({
        url:      '/wall/upload',
        dataType: 'json',
        replaceFileInput: false,
        add:      function( e, data ) {
            var createImage = true;
            if ( !window.URL ) {
                createImage = false;
            } else {
                var fileUrl = window.URL.createObjectURL(data.files[ 0 ]);
            }

            // Check max file size
            if ( data.files[ 0 ].hasOwnProperty('size') && data.files[ 0 ].size > 15000000 ) {
                context.showError(context.props.t('fileupload_placeholder_to_large'));
                return;
            }

            // Set video placeholder if uploading video
            if( context.isVideo(data.files[ 0 ].name) ) {
                context.setState( {preview: '/images/_default_/video-camera-icon.png'} );
            }

            //if ( context.isVideo(data.files[ 0 ].name) ) {
            //    context.showError('Please upload images only');
            //    return;
            //}

            // Show progressbar
            $('.progressbar-container').show();

            // IE return
            if ( !createImage ) {
                context.showMessage(context.props.t('fileupload_file_being_uploaded'));
                data.submit();
                return;
            }

            // Check if file is video
            if ( !context.isVideo(data.files[ 0 ].name) ) {
                if ( fileUrl ) {
                    context.setState({preview: fileUrl});
                } else {
                    context.showMessage(context.props.t('fileupload_file_being_uploaded'));
                }
                data.submit();
                return;
            }

            // File is always a video past this point

            // Create elements to make a video frame image
            var video     = document.createElement('video');
            var canvas    = document.createElement('canvas');
            canvas.width  = 640;
            canvas.height = 400;
            var image     = document.createElement('image');

            // Fallback if the video does not trigger loadeddata event
            var videoTriggered = false;
            setTimeout(function() {
                if ( !videoTriggered ) {
                    videoTriggered = true;
                    data.submit();
                }
            }, 3000);

            video.addEventListener('loadeddata', function() {
                if ( videoTriggered ) {
                    return;
                }
                videoTriggered = true;
                if ( video.duration && video.duration > 60 ) {
                    $('.progressbar-container').hide();
                    context.showError(this.props.t('fileupload_placeholder_to_long'));
                } else {
                    video.currentTime = 1;
                    data.submit();
                }
            }.bind(context), false);

            video.addEventListener('seeked', function() {
                var ctx = canvas.getContext('2d');
                ctx.drawImage(video, 0, 0, 640, 400);
                var imageUrl = canvas.toDataURL();
                if ( imageUrl ) {
                    context.setState({preview: imageUrl});
                } else {
                    context.showMessage(context.props.t('fileupload_file_being_uploaded'));
                }
            }.bind(context));

            video.src = fileUrl;
        }.bind(context),
        done:     function( e, data ) {
            setTimeout(function() {
                $('.progressbar-container').hide();
                if(window.progressbar) window.progressbar.set(0);
                else if(context.progressbar) context.progressbar.set(0);
                if ( data.result.error ) {
                    context.showError(data.result.error.message);
                } else {
                    // Check if the uploaded file is not a video
                    var update = {
                        file_name: data.result.filename,
                        file_path: data.result.filepath,
                        original: data.result.file_original
                    };

                    if ( !context.isVideo(data.files[ 0 ].name) ) {
                        context.showMessage(this.props.t('fileupload_placeholder_upload_successful'));
                        update.preview = data.result.filepath;
                    } else {
                        context.showMessage(this.props.t('fileupload_video_uploaded'));
                    }

                    context.setState(update);
                }
            }.bind(context), 1000);
        }.bind(context),
        fail: function( e, data ) {
            $('.progressbar-container').hide();
            //context.showError(data.errorThrown);
            context.showError(this.props.t('fileupload_placeholder_something_wrong'));
        }.bind(context),
        progress: function( e, data ) {
            if(window.progressbar) window.progressbar.animate((data.loaded / data.total).toFixed(2));
            else if(context.progressbar) context.progressbar.animate((data.loaded / data.total).toFixed(2));
        }.bind(context)
    });
};