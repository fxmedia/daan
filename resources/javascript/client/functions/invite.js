module.exports = {
    signUp: (data, attending, waiting, errors) => {

        _signup('/api/signMeUp',data, attending, waiting, errors);
    },
    signUpAndRegister: (data, attending, waiting, errors) => {
        
        _signup('/api/signUpAndRegister',data, attending, waiting, errors);
    },
    signOut: (done, err, captcha_token) => {
        $.ajax({
            type: 'POST',
            url: '/api/signMeOut',
            data: {token: token, captcha: captcha_token},
            success: result => done(result),
            error: result => err(result)
        });
    },
    /** @TODO fix / check this function & corresponding API function **/
    register: (data, done) => {
        data.token = token;
        $.ajax({
            type: 'POST',
            url: '/api/register',
            data: data,
            success: result => {
                done(result);
            }
        });
    },
    /** @TODO fix / check this function & corresponding API function **/
    resend: (data, done, fail) => {
        data.token = token;
        $.ajax({
            type: 'POST',
            url: '/api/resend',
            data: data,
            success: function (data) {
                done(data.email[0]);
            },
            error: function () {
                fail();
            }
        });
    },
    updateAdditionalInfo: (data, done, errors) => {
        data.token = token;
        $.ajax({
            type: 'POST',
            url: '/api/additionalInfo',
            data: data,
            success: function (data) {
                done(data);
            },
            error: function(error) {
                errors(error);
            }
        })
    }
};

function _signup (url, data, attending, waiting, errors) {
    data.token = token;

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (data) {
            var state = data.is;
            
            switch (state) {
                case 'attending':
                    attending(data);
                    break;
                case 'waiting':
                    waiting(data);
                    break;

            }
        },
        error: function (error) {
            errors(error);
        }
    });
};
