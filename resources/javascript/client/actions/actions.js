module.exports = {

    addPost: function (data) {
        this.dispatch('addPost', data);
    },

    addPosts: function (data) {
        this.dispatch('addPosts', data);
    },

    deletePost: function (post) {
        this.dispatch('deletePost', post);
    },

    setPage: function (page) {
        this.dispatch('setPage', page);
    },

    setSubPage(page) {
        this.dispatch('setSubPage', page);
    },

    setStage(stage) {
        this.dispatch('setStage', stage);
    },

    startQuiz: function (name) {
        this.dispatch('startQuiz', name);
    },

    endQuiz: function () {
        this.dispatch('endQuiz');
    },

    wallInfo: function (data) {
        this.dispatch('wallInfo', data);
    },

    setUser: function (data) {
        this.dispatch('setUser', data);
    },

    setQuestion: function (question) {
        this.dispatch('setQuestion', question);
    },

    validateQuestion: function (answer) {
        this.dispatch('validateClient', answer);
    },

    setName: function (name) {
        this.dispatch('setName', name);
    },

    setAnswer: function (answer) {
        this.dispatch('setAnswer', answer);
    },

    setMyScore: function (score) {
        this.dispatch('setMyScore', score);
    },

    setMyName: function (name) {
        this.dispatch('setMyName', name);
    },

    setScore: function (score) {
        this.dispatch('setScore', score);
    },

    setPushContent: function (data) {
        this.dispatch('setPushContent', data);
    },

    setType: function (type) {
        this.dispatch('setType', type);
    },

    setQuestionFallback: function (question) {
        this.dispatch('setQuestionFallback', question);
    },

    setSettings: function (settings) {
        this.dispatch('setSettings', settings);
    },

    startVoting: function (users) {
        this.dispatch('startVoting', users);
    },

    startQuestionnaire: function (data) {
        this.dispatch('startQuestionnaire', data);
    },

    setQuestionnaireAnswers: function (answers) {
        this.dispatch('setQuestionnaireAnswers', answers);
    },

    emptyQuestionnaires: function () {
        this.dispatch('emptyQuestionnaires', {});
    },

    endQuestionnaire: function (questionnaire) {
        this.dispatch('endQuestionnaire', questionnaire);
    },

    setProgram:       function (data) {
        this.dispatch('setProgram', data);
    },
    setActiveProgram: function (id) {
        this.dispatch('setActive', id);
    },

    oqSetQuestion: function (data) {
        this.dispatch('oqSetQuestion', data);
    },

    oqStop: function () {
        this.dispatch('oqStop');
    },
};