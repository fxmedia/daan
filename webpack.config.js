import Routes from './routes';

export default {
    entry:  Routes.webpack_entry,
    output: Routes.webpack_output,
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env', 'react']
                }
            }
        }]
    },
    plugins: []
}