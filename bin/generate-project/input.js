var required = value => {
    return value && value.length > 0;
};

module.exports = [
    {
        name: 'project-name',
        type: 'input',
        message: 'What is the project name?',
        filter: value => {
            return value.toLowerCase().replace(/ /g, '-');
        },
        validate: required
    },
    {
        name: 'make-git-repo',
        type: 'confirm',
        message: 'Do you want to create a git repository on bitbucket?',
        default: false
    },
    {
        name: 'git-protocol',
        type: 'list',
        message: 'Git Protocol (for cloning)',
        choices: [
            'HTTPS',
            'SSH'
        ]
    },
    {
        name: 'bitbucket-username',
        type: 'input',
        message: 'Bitbucket username',
        validate: required,
        when: answers => {
            return answers['git-protocol'] === 'HTTPS' || answers['make-git-repo'] === true
        }
    },
    {
        name: 'bitbucket-password',
        type: 'password',
        message: 'Bitbucket password',
        validate: required,
        when: answers => {
            return answers['make-git-repo'] === true
        }
    }
];