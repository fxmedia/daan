/*
 |--------------------------------------------------------------------------
 | Application Environment
 |--------------------------------------------------------------------------
 |
 | Setup application variables here
 |
 */
module.exports = {

    /*
     |--------------------------------------------------------------------------
     | (S)FTP Login
     |--------------------------------------------------------------------------
     |
     | Login details for the FTP environments
     |
     */
    FTP_ENV: {
        production: {
            host: '',
            port: 22,
            user: '',
            pass: '',
            remotePath: '/home/{user}/domains/{domain}/public_html/'
        },
        development: {
            host: '',
            port: 22,
            user: '',
            pass: '',
            remotePath: ''
        }
    },

    /*
     |--------------------------------------------------------------------------
     | Folder
     |--------------------------------------------------------------------------
     |
     | Optional sub folder where bundles will be stored on deployment environments
     | False for no folder
     |
     */
    folder: false,

    /*
     |--------------------------------------------------------------------------
     | Image compression
     |--------------------------------------------------------------------------
     |
     | Enable image compression
     |
     */
    imageCompression: true,

    /*
     |--------------------------------------------------------------------------
     | Image compression for dev task
     |--------------------------------------------------------------------------
     |
     | Overwrite the default imageCompression for dev task
     |
     */
    imageCompressionOnDev: false,

    /*
     |--------------------------------------------------------------------------
     | UglifyJS option
     |--------------------------------------------------------------------------
     |
     | Use webpack's UglifyJSPlugin on distribution builds
     |
     */
    uglifyBundles: true,

    /*
     |--------------------------------------------------------------------------
     | Array Browser support queries for autoprefixer
     |--------------------------------------------------------------------------
     |
     | https://github.com/postcss/autoprefixer#options
     |
     */
    cssBrowserSupport: [
        'ie >= 9',
        'last 2 versions'
    ],

    /*
     |--------------------------------------------------------------------------
     | CSS Output Style
     |--------------------------------------------------------------------------
     |
     | Possible output styles: nested, expanded, compact, compressed
     |
     */
    cssOutputStyle: 'compressed'


};