#!/usr/bin/env node --harmony

var inquirer = require('inquirer'),
    input = require('./input'),
    execute = require('child_process').exec,
    request = require('superagent');

class Generator {

    constructor () {
        this.data = null;

        this.inquire();
    }

    inquire() {
        inquirer
            .prompt(input)
            .then(this.promptComplete.bind(this));
    }

    promptComplete(answers) {
        this.data = answers;

        this.createProject()
            .then(result => {
                console.log('Project created');
                if(this.data['make-git-repo'] === false) {
                    return true;
                }

                return this.createRepo()
                    .then(result => {
                        console.log('Repo created');
                        return this.bindProjectToRepo(result.body.full_name);
                    })
                    .then(result => {
                        console.log('Repo remote added, files commited and pushed');
                    });

            })
            .catch(err => {
                console.error('Something somewhere went horribly wrong :[', err);
            });
    }

    createProject() {
        var gitlink = (this.data['git-protocol'] === 'HTTPS')
            ? ['https://', this.data['bitbucket-username'], '@bitbucket.org/fxmedia/frontend.git'].join('')
            : 'git@bitbucket.org:fxmedia/frontend.git';

        return new Promise((resolve, reject) => {
            execute([
                'cd ../',
                ['git clone', gitlink, this.data['project-name']].join(' '),
                ['cd', this.data['project-name']].join(' '),
                'git remote remove origin',
                'cp bin/generate-project/config.js app.config.js'
            ].join('; '), (err, stdout, stderr) => {
                if(err) reject(err);

                console.log(stdout);
                resolve();
            });
        })
    }

    createRepo() {
        return new Promise((resolve, reject) => {
            request
                .post('https://api.bitbucket.org/2.0/repositories/fxmedia/' + this.data['project-name'])
                .auth(this.data['bitbucket-username'], this.data['bitbucket-password'])
                .send({project: {key: 'REP'}})
                .set('Accept', 'application/json')
                .end((err, response) => {
                    if(err) reject(err);
                    resolve(response);
                });
        });
    }

    bindProjectToRepo(fullname) {
        return new Promise((resolve, reject) => {
            console.log('Installing node modules ...');

            execute([
                'cd ../',
                ['cd', this.data['project-name']].join(' '),
                ['git remote add origin ', 'https://', this.data['bitbucket-username'], '@bitbucket.org/', fullname, '.git'].join(''),
                'git add --all',
                'git commit -m "Project created"',
                'git push --set-upstream origin master',
                'npm install -g',
                'npm install -g gulp-cli',
                'npm install',
                'npm link'
            ].join('; '), (err, stdout, stderr) => {
                if(err) reject(err);

                console.log(stdout);
                resolve();
            });
        });
    }

}

new Generator();