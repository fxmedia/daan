var appConfig = require('./../../app.config');

module.exports = [
    {
        name:    'env',
        type:    'list',
        message: 'Deployment Env?',
        choices: [
            'production',
            'development'
        ]
    },
    {
        name: 'bump-version',
        type: 'confirm',
        message: 'Do you want to bump version?',
        default: true
    },
    {
        name: 'version-type',
        type: 'list',
        message: 'Update type',
        choices: [
            'Patch',
            'Minor',
            'Major'
        ],
        when: (answers) => {
            return answers['bump-version'] === true
        }
    },
    {
        name: 'gulp-dist',
        type: 'confirm',
        message: 'Want to run gulp dist?',
        default: true
    },
    {
        name: 'current-folder',
        type: 'confirm',
        message: 'Deploy to subfolder [' + appConfig.folder + '] ?'
    },
    {
        name: 'folder',
        type: 'input',
        message: 'Specify folder name',
        when: answers => {
            return answers['current-folder'] === false
        }
    }
];