#!/usr/bin/env node --harmony

var inquirer    = require('inquirer'),
    input       = require('./input'),
    execute     = require('child_process').exec,
    Client      = require('ssh2-sftp-client'),
    sftp        = new Client(),
    pkg         = require('./../../package.json'),
    appConfig   = require('./../../app.config'),
    path        = require('path'),
    fs          = require('fs'),
    Promise     = require('bluebird'),
    ProgressBar = require('progress');

class Deployment {

    constructor () {
        this.data = null;

        this.inquire();
    }

    inquire () {
        console.log('inquire');

        inquirer
            .prompt(input)
            .then(this.promptComplete.bind(this));
    }

    promptComplete (answers) {
        this.data = answers;

        if (
            !appConfig.FTP_ENV[ this.data[ 'env' ] ].host ||
            !appConfig.FTP_ENV[ this.data[ 'env' ] ].user ||
            !appConfig.FTP_ENV[ this.data[ 'env' ] ].pass ||
            !appConfig.FTP_ENV[ this.data[ 'env' ] ].remotePath
        ) {
            return console.log(`Missing FTP details for ${this.data[ 'env' ]} env`);
        }

        let chain = Promise.resolve();

        if ( this.data[ 'gulp-dist' ] ) {
            chain.then(this.buildAssets);
        }

        if ( this.data[ 'bump-version' ] ) {
            return chain.then(this.bumpVersion.bind(this))
                .catch(() => {
                    return this.inquireCommit()
                        .then(answers => {
                            Object.assign(this.data, answers);
                        })
                        .then(() => {
                            if ( this.data[ 'add-files' ] === true ) {
                                return this.commitChanges(this.data[ 'commit-message' ])
                                    .then(this.bumpVersion.bind(this));
                            }

                            throw new Error('We\'re not committing changes');
                        });
                })
                .then(this.deploy.bind(this))
                .catch(err => {
                    console.log('Cancelling deployment', err);
                });
        }

        chain.then(this.deploy.bind(this));
    }

    inquireCommit () {
        return inquirer.prompt([ {
            name:    'add-files',
            message: 'Whoops, git working directory is not clean. Do you want to add all changes?',
            type:    'confirm',
        }, {
            name:    'commit-message',
            message: 'Commit message',
            type:    'input',
            when:    answers => {
                return answers[ 'add-files' ] === true
            }
        } ]);
    }

    buildAssets () {
        console.log('Building assets ..');

        return new Promise((resolve, reject) => {
            execute('gulp dist;', (err, stdout, stderr) => {
                if ( stderr || err ) reject(stderr);

                resolve(stdout);
            });
        });
    }

    bumpVersion () {
        console.log('Bumping version ..');

        return new Promise((resolve, reject) => {
            execute('npm version ' + this.data[ 'version-type' ].toLowerCase(), (err, stdout, stderr) => {
                if ( err || stderr ) reject(stderr);

                resolve(stdout);
            });
        });
    }

    commitChanges (message) {
        console.log('Committing changes ..');

        return new Promise((resolve, reject) => {
            execute(`git add --all; git commit -m "${message}";`, (err, stdout, stderr) => {
                if ( err || stderr ) reject(stderr);

                resolve(stdout);
            });
        })
    }

    getImages () {
        var fileTree = [];

        function getFilesRecursive (folder) {
            var fileContents = fs.readdirSync(folder),
                stats;

            fileContents.forEach(function (fileName) {
                stats = fs.lstatSync(folder + '/' + fileName);

                if ( stats.isDirectory() ) {
                    getFilesRecursive(folder + '/' + fileName)
                } else {
                    var fullpath = path.join(__dirname, "../../dist/images");
                    var chunks   = folder.split(fullpath);
                    var _folder  = chunks[ 1 ] || '/';

                    fileTree.push({
                        filename: fileName,
                        path:     folder + '/' + fileName,
                        folder:   _folder
                    });
                }
            });
        }

        getFilesRecursive(path.join(__dirname, "../../dist/images"));

        return fileTree;
    }

    deploy () {
        console.log('Starting deployment, opening SFTP connection ..');

        var folder = this.data[ 'current-folder' ] === true
            ? appConfig.folder
            : this.data[ 'folder' ];

        var basepath = appConfig.FTP_ENV[ this.data[ 'env' ] ].remotePath + folder;

        let filesToDeploy = [
                {
                    inputFile:  '/javascript/build.client.js',
                    outputFile: '/client.' + pkg.version + '.js',
                    subfolder:  'javascript'
                },
                {
                    inputFile:  '/javascript/build.client.js',
                    outputFile: '/client.latest.js',
                    subfolder:  'javascript'
                },
                {
                    inputFile:  '/stylesheets/main.css',
                    outputFile: '/style.' + pkg.version + '.css',
                    subfolder:  'stylesheets'
                },
                {
                    inputFile:  '/stylesheets/main.css',
                    outputFile: '/style.latest.css',
                    subfolder:  'stylesheets'
                },
                {
                    inputFile:  '/stylesheets/main.css.map',
                    outputFile: '/style.latest.css.map',
                    subfolder:  'stylesheets'
                }
            ],
            images        = this.getImages(),
            progressTotal =
                filesToDeploy.length +
                images.length +
                3; // Other steps

        var progressBar = new ProgressBar(':bar', {total: progressTotal});

        sftp.connect({
            host:     appConfig.FTP_ENV[ this.data[ 'env' ] ].host,
            port:     appConfig.FTP_ENV[ this.data[ 'env' ] ].port,
            username: appConfig.FTP_ENV[ this.data[ 'env' ] ].user,
            password: appConfig.FTP_ENV[ this.data[ 'env' ] ].pass
        }).then(() => {
            progressBar.tick();

            return sftp.list(basepath).catch(() => {
                return sftp.mkdir(basepath);
            });

        }).then(() => {
            progressBar.tick();

            let fileOperations = filesToDeploy.map(fileObject => {

                return function () {
                    var filePath  = path.join(__dirname, "../../dist" + fileObject.inputFile);
                    var stream    = fs.createReadStream(filePath);
                    var subfolder = fileObject.subfolder ? fileObject.subfolder + '/' : '';

                    return sftp.list(basepath + '/' + subfolder).catch(() => {
                        return sftp.mkdir(basepath + '/' + subfolder, true);
                    }).then(() => {
                        progressBar.tick();
                        return sftp.put(stream, basepath + '/' + fileObject.subfolder + fileObject.outputFile);
                    });
                }

            });
            return Promise.each(fileOperations, value => value());
        }).then(() => {
            progressBar.tick();

            let fileOperations = this.getImages().map(image => {
                return function () {
                    var stream = fs.createReadStream(image.path);

                    return sftp.list(basepath + '/images' + image.folder).catch(() => {
                        return sftp.mkdir(basepath + '/images' + image.folder, true);
                    }).then(() => {
                        progressBar.tick();
                        return sftp.put(stream, basepath + '/images' + image.folder + '/' + image.filename);
                    });
                }
            });

            return Promise.each(fileOperations, value => value());
        }).then(() => {
            progressBar.tick();
            return sftp.end();
        }).then(() => {
            console.log('Deployment finished successfully');

            progressBar.tick();
        }).catch((err) => {
            console.error('Whoops, something went wrong', err);
        });
    }

}

new Deployment();