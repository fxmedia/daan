import AppConfig from './app.config';
import Routes from './routes';
import webpackConfig from './webpack.config.js'

import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import imagemin from 'gulp-imagemin';
import gulpif from 'gulp-if';
import webpack from 'webpack';
import gutil from 'gutil';
import del from 'del';

const PRODUCTION = 1,
      DEVELOPMENT = 2;

let ENV = PRODUCTION;

const _production = ready => { ENV = PRODUCTION; ready(); };
const _development = ready => { ENV = DEVELOPMENT; ready(); };

export function images() {
    let compress = (ENV === DEVELOPMENT)
        ? AppConfig.imageCompressionOnDev
        : AppConfig.imageCompression;

    return gulp.src(Routes.IMAGES)
        .pipe(gulpif(compress, imagemin()))
        .pipe(gulp.dest(Routes.IMAGE_DEST));
}

export function styling() {
    return gulp.src(Routes.SASS_FILES)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: AppConfig.cssOutputStyle})
            .on('error', sass.logError))
        .pipe(autoprefixer({browsers: AppConfig.cssBrowserSupport}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(Routes.STYLESHEET_DEST));
}

export function bundles(ready) {
    if(ENV === PRODUCTION) {
        if(AppConfig.uglifyBundles) {
            webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin());
        }
        webpackConfig.plugins.push(new webpack.optimize.DedupePlugin());
    }

    webpack(webpackConfig, (err, stats) => {
        if ( err ) throw new gutil.PluginError("webpack", err);

        ready();
    });

}

export function clean() {
    return del(['dist/images', 'dist/stylesheets', 'dist/javascript']);
}

export function watch() {
    gulp.watch(Routes.SASS_FILES, styling);
    gulp.watch(Routes.IMAGES, images);
    gulp.watch(Routes.JAVASCRIPT_FILES, bundles);
}

const build = gulp.parallel(styling, images, bundles);
const dev = gulp.series(_development, build, watch);
const dist = gulp.series(_production, build);


export {
    dev, dist, build
}

export default dev;